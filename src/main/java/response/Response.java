package response;

import org.apache.commons.collections.map.HashedMap;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by dangdien on 7/5/17.
 */
public class Response {
    public enum ResponseStatus {
        SUCCESS,
        ERROR
    }

    public Response(ResponseStatus status) {
        Map<String, Object> data = new HashMap<String, Object>();
        this.setData(data);
        this.status = status;
    }

    public Response() {
        Map<String, Object> data = new HashMap<String, Object>();
        this.setData(data);
    }

    protected ResponseStatus status;
    private String message;
    private Map<String, Object> data;

    public ResponseStatus getStatus() {
        return status;
    }

    public void setStatus(ResponseStatus status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Map<String, Object> getData() {
        return data;
    }

    public void setData(Map<String, Object> data) {
        this.data = data;
    }


    @Override
    public String toString() {
        return "Response{" +
                "status=" + status +
                ", message='" + message + '\'' +
                ", data=" + data +
                '}';
    }
}
