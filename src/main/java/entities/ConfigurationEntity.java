package entities;

import javax.persistence.*;

@Entity
@Table(name="configuration")
public class ConfigurationEntity extends AbstractEntity {
  @Basic
  @Column(name = "name")
  private String name;

  @Basic
  @Column(name = "value")
  private String value;

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getValue() {
    return value;
  }

  public void setValue(String value) {
    this.value = value;
  }

  @Override
  public String toString() {
    return "ConfigurationEntity{" +
            "name='" + name + '\'' +
            ", value='" + value + '\'' +
            '}';
  }
}
