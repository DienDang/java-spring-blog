package entities;


import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;

@Entity
@Table(name = "user")
public class UserEntity extends AbstractEntity {

  @Basic
  @Column(name = "username")
  private String username;

  @JsonIgnore
  @Basic
  @Column(name = "password")
  private String password;

  @Basic
  @Column(name = "enabled")
  private boolean enabled;

  @Override
  public String toString() {
    return "UserEntity{" +
            "username='" + username + '\'' +
            ", password='" + password + '\'' +
            ", enabled=" + enabled +
            '}';
  }

  public String getUsername() {
    return username;
  }

  public void setUsername(String username) {
    this.username = username;
  }

  public String getPassword() {
    return password;
  }

  public void setPassword(String password) {
    this.password = password;
  }

  public boolean isEnabled() {
    return enabled;
  }

  public void setEnabled(boolean enabled) {
    this.enabled = enabled;
  }
}
