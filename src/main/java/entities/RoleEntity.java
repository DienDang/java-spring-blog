package entities;

import javax.persistence.*;

/**
 * Created by dangdien on 6/8/17.
 */
@Entity
@Table(name = "role")
public class RoleEntity extends AbstractEntity {

    @OneToOne
    @JoinColumn(name = "user_id")
    private UserEntity user;

    @Basic
    @Column(name = "role")
    private String role;

    @Override
    public String toString() {
        return "RoleEntity{" +
                "user=" + user +
                ", role='" + role + '\'' +
                '}';
    }

    public UserEntity getUser() {
        return user;
    }

    public void setUser(UserEntity user) {
        this.user = user;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

}
