package entities;


import javax.persistence.*;
import java.util.Date;

/**
 * Created by dangdien on 5/5/17.
 */

@Entity
@Table(name="post")
public class PostEntity extends AbstractEntity {

  @Basic
  @Column(name = "title")
  private String title;

  @Basic
  @Column(name = "content")
  private String content;

  @Basic
  @Column(name = "post_time")
  private Date postTime;

  @Basic
  @Column(name = "no_view")
  private int noView;

  @Basic
  @Column(name = "no_like")
  private int noLike;

  @ManyToOne(cascade = CascadeType.DETACH, fetch = FetchType.EAGER)
  @JoinColumn(name = "user_id", referencedColumnName = "id")
  private UserEntity author;

  @Basic
  @Column(name = "status")
  private boolean status;

  @Basic
  @Column(name = "approve")
  private boolean approve;

  @Basic
  @Column(name = "approved_time")
  private Date approvedTime;

  @Basic
  @Column(name = "last_update_time")
  private Date lastUpdate;

  @Basic
  @Column(name = "last_update_author")
  private String lastUpdateAuthor;

  @Basic
  @Column(name = "image_link")
  private String imageLink;

  @Override
  public String toString() {
    return "PostEntity{" +
            "title='" + title + '\'' +
            ", content='" + content + '\'' +
            ", postTime=" + postTime +
            ", noView=" + noView +
            ", noLike=" + noLike +
            ", author=" + author +
            ", status=" + status +
            ", approve=" + approve +
            ", approvedTime=" + approvedTime +
            ", lastUpdate=" + lastUpdate +
            ", lastUpdateAuthor='" + lastUpdateAuthor + '\'' +
            ", imageLink='" + imageLink + '\'' +
            '}';
  }

  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public String getContent() {
    return content;
  }

  public void setContent(String content) {
    this.content = content;
  }

  public Date getPostTime() {
    return postTime;
  }

  public void setPostTime(Date postTime) {
    this.postTime = postTime;
  }

  public int getNoView() {
    return noView;
  }

  public void setNoView(int noView) {
    this.noView = noView;
  }

  public int getNoLike() {
    return noLike;
  }

  public void setNoLike(int noLike) {
    this.noLike = noLike;
  }

  public UserEntity getAuthor() {
    return author;
  }

  public void setAuthor(UserEntity author) {
    this.author = author;
  }

  public boolean isStatus() {
    return status;
  }

  public void setStatus(boolean status) {
    this.status = status;
  }

  public boolean isApprove() {
    return approve;
  }

  public void setApprove(boolean approve) {
    this.approve = approve;
  }

  public Date getApprovedTime() {
    return approvedTime;
  }

  public void setApprovedTime(Date approvedTime) {
    this.approvedTime = approvedTime;
  }

  public Date getLastUpdate() {
    return lastUpdate;
  }

  public void setLastUpdate(Date lastUpdate) {
    this.lastUpdate = lastUpdate;
  }

  public String getLastUpdateAuthor() {
    return lastUpdateAuthor;
  }

  public void setLastUpdateAuthor(String lastUpdateAuthor) {
    this.lastUpdateAuthor = lastUpdateAuthor;
  }

  public String getImageLink() {
    return imageLink;
  }

  public void setImageLink(String imageLink) {
    this.imageLink = imageLink;
  }
}

