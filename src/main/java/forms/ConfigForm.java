package forms;

import entities.ConfigurationEntity;

import java.util.List;

/**
 * Created by dangdien on 6/28/17.
 */
public class ConfigForm {
    private List<ConfigurationEntity> config;

    public List<ConfigurationEntity> getConfig() {
        return config;
    }

    public void setConfig(List<ConfigurationEntity> config) {
        this.config = config;
    }

    @Override
    public String toString() {
        return "ConfigForm{" +
                "config=" + config +
                '}';
    }
}
