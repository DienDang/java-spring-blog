package request;

/**
 * Created by dangdien on 7/5/17.
 */
public class Pagination {
    protected int limit;
    protected String orderBy;
    protected String orderMode;
    protected int offset;

    public Pagination() {
    }

    public Pagination(int limit, String orderBy, String orderMode, int offset) {
        this.limit = limit;
        this.orderBy = orderBy;
        this.orderMode = orderMode;
        this.offset = offset;
    }

    public int getLimit() {
        return limit;
    }

    public void setLimit(int limit) {
        this.limit = limit;
    }

    public String getOrderBy() {
        return orderBy;
    }

    public void setOrderBy(String orderBy) {
        this.orderBy = orderBy;
    }

    public String getOrderMode() {
        return orderMode;
    }

    public void setOrderMode(String orderMode) {
        this.orderMode = orderMode;
    }

    public int getOffset() {
        return offset;
    }

    public void setOffset(int offset) {
        this.offset = offset;
    }

    @Override
    public String toString() {
        return "Pagination{" +
                "limit=" + limit +
                ", orderBy='" + orderBy + '\'' +
                ", orderMode='" + orderMode + '\'' +
                ", offset=" + offset +
                '}';
    }
}
