package exception;

import com.sun.org.apache.regexp.internal.RE;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import response.Response;

/**
 * Created by dangdien on 7/8/17.
 */
@ControllerAdvice
public class GlobalExceptionHandler {
    @ExceptionHandler(Exception.class)
    public @ResponseBody Response exception(Exception e) {
        Response response = new Response(Response.ResponseStatus.ERROR);
        System.out.println("ERROR : " + e.getClass() + " : " + e.getMessage());
        response.setMessage("ERROR");
        return response;
    }
}
