package exception;

/**
 * Created by dangdien on 7/7/17.
 */
public class NameNotChangedExeption extends RuntimeException{
    public NameNotChangedExeption(String msg) {
        super(msg);
    }
}
