package daoimpls;

import dao.UserDAO;
import entities.UserEntity;
import exception.NameNotChangedExeption;
import org.hibernate.Session;
import org.hibernate.query.Query;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.Table;

/**
 * Created by dangdien on 5/6/17.
 */

@Component
@Transactional
public class UserImpls extends AbstractImpls<UserEntity> implements UserDAO {
    public Class<UserEntity> getTypeClass() {
        return UserEntity.class;
    }
    public String getTableName() {
        Table table = UserEntity.class.getAnnotation(Table.class);
        return table.name();
    }

    public UserEntity findUserByUsername(String name) {
        Session session = this.getSessionFactory().getCurrentSession();
        Query<UserEntity> query = session.createQuery("FROM UserEntity where username = :name", UserEntity.class);
        query.setParameter("name", name);
        return query.getSingleResult();
    }


    @Override
    public UserEntity createUser(String username, String password, String role) {
        UserEntity userEntity = new UserEntity();
        userEntity.setUsername(username);
        userEntity.setPassword(password);
        userEntity.setEnabled(false);
        this.save(userEntity);
        return userEntity;
    }

    @Override
    public void checkUsername(String username) {
        String sql = "select * from user where username = :username";
        Session session = this.getSessionFactory().getCurrentSession();
        Query query = session.createNativeQuery(sql, UserEntity.class);
        query.setParameter("username", username);
        query.getSingleResult();
    }

    @Override
    public void checkUpdateUsername(int id, String username) {
        UserEntity user = this.find(id);
        if(user.getUsername().equals(username)) {
            throw new NameNotChangedExeption("name not changed");
        } //check if username given by user id = username (case username not changed)
        else {
            String sql = "select * from user where username = :username ";
            Session session = this.getSessionFactory().getCurrentSession();
            Query query = session.createNativeQuery(sql, UserEntity.class);
            query.setParameter("username", username);
            query.getSingleResult();
        }
    }

    @Override
    public void activeUser(int id, boolean status) {
        UserEntity user = this.find(id);
        user.setEnabled(status);
        this.save(user);
    }
}
