package daoimpls;

import dao.RoleDAO;
import entities.PostEntity;
import entities.RoleEntity;
import entities.UserEntity;
import org.hibernate.Session;
import org.springframework.stereotype.Component;

import javax.persistence.Query;
import javax.persistence.Table;
import java.util.List;

/**
 * Created by dangdien on 6/8/17.
 */

@Component
public class RoleImpls extends AbstractImpls<RoleEntity> implements RoleDAO{
    public Class<RoleEntity> getTypeClass() {
        return RoleEntity.class;
    }

    public String getTableName() {
        Table table = RoleEntity.class.getAnnotation(Table.class);
        return table.name();
    }

    @Override
    public void createRoleForUser(UserEntity user, String role) {
        RoleEntity roleEntity = new RoleEntity();
        roleEntity.setUser(user);
        roleEntity.setRole(role);
        this.save(roleEntity);
    }


    @Override
    public void updateRoleForUser(UserEntity user, String role) {
        String sql = "select * from role where user_id = :id";
        Session session = this.getSessionFactory().getCurrentSession();
        Query query = session.createNativeQuery(sql, RoleEntity.class);
        query.setParameter("id", user.getId());
        RoleEntity roleEntity = (RoleEntity) query.getResultList().get(0);
        roleEntity.setRole(role);
        this.save(roleEntity);
    }

    @Override
    public List<RoleEntity> listRoleByOrder(String orderBy, String orderMode) {
        String sql = "select role.* from role inner join user on role.user_id = user.id order by " + orderBy + " " + orderMode;
        Session session = this.getSessionFactory().getCurrentSession();
        Query query = session.createNativeQuery(sql, RoleEntity.class);
        return query.getResultList();
    }

    @Override
    public List<RoleEntity> searchUser(String orderBy, String orderMode, String keyword) {
        String sql = "select role.* from role inner join user on user_id = user.id where username like :keyword order by " + orderBy + " " + orderMode;
        Session session = this.getSessionFactory().getCurrentSession();
        Query query = session.createNativeQuery(sql, RoleEntity.class);
        query.setParameter("keyword", keyword);
        return query.getResultList();
    }
}
