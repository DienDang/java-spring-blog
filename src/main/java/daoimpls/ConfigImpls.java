package daoimpls;
import dao.ConfigDAO;

import entities.ConfigurationEntity;
import entities.PostEntity;
import javafx.geometry.Pos;
import org.hibernate.Session;
import org.hibernate.query.Query;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.Table;
import java.util.List;

/**
 * Created by dangdien on 5/6/17.
 */
@Component
public class ConfigImpls extends AbstractImpls<ConfigurationEntity> implements ConfigDAO {


//    @Transactional
//    public List<ConfigurationEntity> getPageConfig() {
//        Session mySession = this.getSessionFactory().getCurrentSession();
//        String hql = "FROM ConfigurationEntity";
//        Query<ConfigurationEntity> query = mySession.createQuery(hql, ConfigurationEntity.class);
//        return query.getResultList();
//    }

    public Class<ConfigurationEntity> getTypeClass() {
        return ConfigurationEntity.class;
    }

    public String getTableName() {
        Table table = ConfigurationEntity.class.getAnnotation(Table.class);
        return table.name();
    }

    @Override
    public void saveConfig(ConfigurationEntity config) {
        Session session = this.getSessionFactory().getCurrentSession();
        try {
            String sql = "update configuration set value = :value where name = :name";
            Query query = session.createNativeQuery(sql, PostEntity.class);
            query.setParameter("name", config.getName());
            query.setParameter("value", config.getValue());
            query.executeUpdate();
        }
        catch (Exception e) {
            String sql = "insert into configuration (name, value) values (:name, :value)";
            Query query = session.createNativeQuery(sql, PostEntity.class);
            query.setParameter("name", config.getName());
            query.setParameter("value", config.getValue());
            query.executeUpdate();
        }
    }
}
