package daoimpls;

import dao.PostDAO;
import entities.PostEntity;
import entities.UserEntity;
import org.hibernate.Session;
import org.springframework.stereotype.Component;
import request.Pagination;

import javax.persistence.Query;
import javax.persistence.Table;
import java.math.BigInteger;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by dangdien on 5/6/17.
 */

@Component
public class PostImpls extends AbstractImpls<PostEntity> implements PostDAO {
    public Class<PostEntity> getTypeClass() {
        return PostEntity.class;
    }

    public String getTableName() {
        Table table = PostEntity.class.getAnnotation(Table.class);
        return table.name();
    }

    @Override
    public List<PostEntity> getAllPostWithLimitation(Pagination pagination) {
        String sql = "select post.* from post inner join user on post.user_id = user.id  order by " + pagination.getOrderBy() + " " + pagination.getOrderMode() + " limit :offset, :limit";
        Session session = this.getSessionFactory().getCurrentSession();
        Query query = session.createNativeQuery(sql, PostEntity.class);
        query.setParameter("offset", pagination.getOffset());
        query.setParameter("limit", pagination.getLimit());
        return query.getResultList();
    }

    @Override
    public List<PostEntity> getAllApprovedPost(Pagination pagination) {
        String sql = "from " + this.getClassName() + " where approve = 1 and status = 1" + " ORDER BY " + pagination.getOrderBy() + " " + pagination.getOrderMode() ;
        Session session = this.getSessionFactory().getCurrentSession();
        Query query = session.createQuery(sql, PostEntity.class);
        query.setFirstResult(pagination.getOffset());
        query.setMaxResults(pagination.getLimit());
        return query.getResultList();
    }

    @Override
    public List<PostEntity> getAllPostOfUser(Pagination pagination, String username) {
        String sql = "select post.* from post inner join user on post.user_id = user.id where user.username = :username ORDER BY " + pagination.getOrderBy() + " " + pagination.getOrderMode() + " LIMIT " + pagination.getOffset() + ", " + pagination.getLimit();
        Session session = this.getSessionFactory().getCurrentSession();
        Query query = session.createNativeQuery(sql, PostEntity.class);
        query.setParameter("username", username);
        return query.getResultList();
    }

    @Override
    public PostEntity getSinglePost(int id) {
        return this.find(id);
    }

    @Override
    public void changeApprove(int id, boolean approve) {
        PostEntity post = this.find(id);
        post.setApprove(approve ? true : false);
    }

    @Override
    public void increaseView(int id) {
        String sql = "update post set no_view = no_view + 1 where id = :id";
        Session session = this.getSessionFactory().getCurrentSession();
        Query query = session.createNativeQuery(sql, PostEntity.class);
        query.setParameter("id", id);
        query.executeUpdate();
    }

    @Override
    public int[] postStatistic() {
        int[] array = new int[2];
        Session session = this.getSessionFactory().getCurrentSession();
        String sql = "select count(id) from post where approve = 1";
        String sql2 = "select count(id) from post where approve = 0";
        Query query = session.createNativeQuery(sql);
        Query query2 = session.createNativeQuery(sql2);
        array[0] = ((BigInteger) query.getSingleResult()).intValue();
        array[1] = ((BigInteger) query2.getSingleResult()).intValue();
        return array;
    }

    @Override
    public List<PostEntity> searchByTitle(Pagination pagination, String keyword) {
        String sql = "select * from post where title like :keyword order by " + pagination.getOrderBy() + " " + pagination.getOrderMode() + " limit :offset, :limit";
        Session session = this.getSessionFactory().getCurrentSession();
        Query query = session.createNativeQuery(sql, PostEntity.class);
        query.setParameter("keyword", keyword);
        query.setParameter("offset", pagination.getOffset());
        query.setParameter("limit", pagination.getLimit());
        return query.getResultList();
    }

    @Override
    public List<PostEntity> searchByAuthor(Pagination pagination, String keyword) {
        String sql = "select post.* from post inner join user on post.user_id = user.id where username like :keyword ORDER BY " + pagination.getOrderBy() + " " + pagination.getOrderMode() + " LIMIT " + pagination.getOffset() + ", " + pagination.getLimit();
        Session session = this.getSessionFactory().getCurrentSession();
        Query query = session.createNativeQuery(sql, PostEntity.class);
        query.setParameter("keyword", keyword);
        return query.getResultList();
    }

    @Override
    public List<PostEntity> searchUserPost(Pagination pagination, String username, String keyword) {
        String sql = "select post.* from post inner join user on post.user_id = user.id where username = :username and title like :keyword order by " + pagination.getOrderBy() + " " + pagination.getOrderMode() + " limit :offset, :limit";
        Session session = this.getSessionFactory().getCurrentSession();
        Query query = session.createNativeQuery(sql, PostEntity.class);
        query.setParameter("keyword", keyword);
        query.setParameter("username", username);
        query.setParameter("offset", pagination.getOffset());
        query.setParameter("limit", pagination.getLimit());
        return query.getResultList();
    }

    @Override
    public Map<String, Object> searchPublicApprovedPosts(Pagination pagination, String keyword) { //ERROR! => Not return number of result size yet!
        String sql = "select post.* from post where approve = 1 and status = 1 and title like :keyword order by " + pagination.getOrderBy() + " " + pagination.getOrderMode() + " limit :offset, :limit";
        Session session = this.getSessionFactory().getCurrentSession();
        Query query = session.createNativeQuery(sql, PostEntity.class);
        query.setParameter("keyword", keyword);
        query.setParameter("offset", pagination.getOffset());
        query.setParameter("limit", pagination.getLimit());
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("list", query.getResultList());
        String sql2 = "select count(post.id) from post inner join user on post.user_id = user.id where approve = 1 and status = 1 and title like :keyword";
        Query query1 = session.createNativeQuery(sql2);
        query1.setParameter("keyword", keyword);
        map.put("count", query1.getSingleResult());
        return map;
    }

    @Override
    public List<PostEntity> getAllPostOfAuthor(Pagination pagination, String username) {
        String sql = "select post.* from post inner join user on post.user_id = user.id where user.username = :username and status = 1 and approve = 1 ORDER BY " + pagination.getOrderBy() + " " + pagination.getOrderMode() + " LIMIT " + pagination.getOffset() + ", " + pagination.getLimit();
        Session session = this.getSessionFactory().getCurrentSession();
        Query query = session.createNativeQuery(sql, PostEntity.class);
        query.setParameter("username", username);
        return query.getResultList();
    }

    @Override
    public List<PostEntity> searchAuthorPost(Pagination pagination, String username, String keyword) {
        String sql = "select post.* from post inner join user on post.user_id = user.id where username = :username and status = 1 and approve = 1 and title like :keyword order by " + pagination.getOrderBy() + " " + pagination.getOrderMode() + " limit :offset, :limit";
        Session session = this.getSessionFactory().getCurrentSession();
        Query query = session.createNativeQuery(sql, PostEntity.class);
        query.setParameter("keyword", keyword);
        query.setParameter("username", username);
        query.setParameter("offset", pagination.getOffset());
        query.setParameter("limit", pagination.getLimit());
        return query.getResultList();
    }

    @Override
    public List<PostEntity> findAllPostsByUsernameWithoutLimitation(String username) {
        String sql = "select post.* from post inner join user on post.user_id = user.id where username = :username";
        Session session = this.getSessionFactory().getCurrentSession();
        Query query = session.createNativeQuery(sql, PostEntity.class);
        query.setParameter("username", username);
        return query.getResultList();
    }

    @Override
    public PostEntity getPostOfUser(int id, UserEntity user) {
        String sql = "select * from post where id = :id and user_id = :userId";
        Session session = this.getSessionFactory().getCurrentSession();
        Query query = session.createNativeQuery(sql, PostEntity.class);
        query.setParameter("userId", user.getId());
        return (PostEntity) query.getSingleResult();
    }
}
