package daoimpls;


import dao.AbstractDAO;
import entities.AbstractEntity;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;


import java.util.List;

/**
 * Created by dangdien on 6/8/17.
 */
@Transactional
public abstract class AbstractImpls<E extends AbstractEntity> implements AbstractDAO<E> {
    @Autowired
    private SessionFactory mySessionFactory;
    public SessionFactory getSessionFactory() {
        return mySessionFactory;
    }

    public void setSessionFactory(SessionFactory sessionFactory) {
        this.mySessionFactory = sessionFactory;
    }

    public E find(int id) {
        Session session = this.getSessionFactory().getCurrentSession();
        E object = session.find(this.getTypeClass(), id);
        return object;
    }

    public void delete(int id) {
        Session session = this.getSessionFactory().getCurrentSession();
        E object = this.find(id);
        session.delete(object);
    }

    public void save(E e) {
        Session session = this.getSessionFactory().getCurrentSession();
        session.saveOrUpdate(e);

    }

    public List<E> findAll() {
        Session session = this.getSessionFactory().getCurrentSession();
        Query<E> query = session.createQuery("from " + this.getTypeClass().getName(), this.getTypeClass());
        return query.getResultList();
    }

    public abstract Class<E> getTypeClass();

    public abstract String getTableName();

    protected String getClassName() {
        return getTypeClass().getName();
    }

}
