package services;

import dao.RoleDAO;
import entities.RoleEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Created by dangdien on 6/20/17.
 */

@Component
public class RoleService {
    @Autowired
    RoleDAO roleDAO;

    public List<RoleEntity> findAll() {
        return roleDAO.findAll();
    }

    public List<RoleEntity> listRoleByOrder(String orderBy, String orderMode) {
        return roleDAO.listRoleByOrder(orderBy, orderMode);
    }

    public List<RoleEntity> searchUser(String orderBy, String orderMode, String keyword) {
        return roleDAO.searchUser(orderBy, orderMode, keyword);
    }
}
