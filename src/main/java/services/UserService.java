package services;

import dao.RoleDAO;
import dao.UserDAO;
import entities.UserEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.NoResultException;
import java.util.List;

/**
 * Created by dangdien on 6/20/17.
 */

@Component
@Transactional
public class UserService {

    @Autowired
    UserDAO userDAO;

    @Autowired
    PasswordEncoder bCryptPasswordEncoder;

    @Autowired
    RoleDAO roleDAO;

    public UserEntity getUserbyUsername(String name) {
        return userDAO.findUserByUsername(name);
    }

    public UserEntity find(int id) {
        return userDAO.find(id);
    }

    public void delete(int id) {
        userDAO.delete(id);
    }

    public void save(UserEntity userEntity) {
         userDAO.save(userEntity);
    }

    public List<UserEntity> findAll() {
        return userDAO.findAll();
    }


    public void createUser(String username, String password, String role) {
        String encodedPassword = bCryptPasswordEncoder.encode(password);
        try {
            UserEntity ue = userDAO.findUserByUsername(username);
            throw new RuntimeException("USERNAME_TAKEN_ERROR");
        }
        catch (NoResultException e) {
            try {
                UserEntity userEntity = userDAO.createUser(username, encodedPassword, role);
                roleDAO.createRoleForUser(userEntity, role);
            } catch (Exception e1) {
                throw new RuntimeException("ERROR");
            }
        }
    }

    public void updateUser(int id, String username, String password, String role) {
        UserEntity user = this.find(id);
        if(!username.equals(user.getUsername())) { //username changed => check new username
            try {
                UserEntity ue = userDAO.findUserByUsername(username);
                throw new RuntimeException("USERNAME_TAKEN_ERROR");
            } catch (NoResultException e) {
                    user.setUsername(username);
            }
        }
        if(password != null) {
            try {
                String encodedPassword = bCryptPasswordEncoder.encode(password); //change password if password parameter != null
                user.setPassword(encodedPassword);
                roleDAO.updateRoleForUser(user, role);
                userDAO.save(user);
            }
            catch (Exception e) {
                throw new RuntimeException("ERROR");
            }
        }
    }

    public void activeUser(int id, boolean status) {
        userDAO.activeUser(id, status);
    }

    public void changePasswordForUser(int id, String oldPassword, String password) {
        UserEntity user = this.find(id);
        if(password != null) {
            if(!bCryptPasswordEncoder.matches(oldPassword, user.getPassword())){
                throw new RuntimeException("CURRENT_PASSWORD_ERROR");
            }
            String encodedNewPassword = bCryptPasswordEncoder.encode(password); //change password if password parameter != null
            user.setPassword(encodedNewPassword);
            userDAO.save(user);
        }
        else {
            throw new RuntimeException("BLANK_FIELDS_ERROR");
        }
    }
}
