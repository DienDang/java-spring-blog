package services;

import dao.ConfigDAO;
import daoimpls.ConfigImpls;
import entities.ConfigurationEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Created by dangdien on 6/20/17.
 */
@Component
public class ConfigService {

    @Autowired
    private ConfigDAO configDAO;


    public List<ConfigurationEntity> findAll() {
        return configDAO.findAll();
    }


    public void save(List<ConfigurationEntity> config) {
        for(ConfigurationEntity element : config) {
            configDAO.saveConfig(element);
        }
    }
}
