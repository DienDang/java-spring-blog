package services;

import dao.PostDAO;
import entities.PostEntity;
import entities.UserEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import request.Pagination;

import java.util.List;
import java.util.Map;

/**
 * Created by dangdien on 6/20/17.
 */
@Controller
public class PostService {

    @Autowired
    PostDAO postDAO;

    public PostEntity find(int id) {
        return postDAO.find(id);
    }

    public void delete(int id) {
        postDAO.delete(id);
    }

    public void save(PostEntity postEntity) {
        postDAO.save(postEntity);
    }

    public List<PostEntity> findAll() {
        return postDAO.findAll();
    }

    public List<PostEntity> getAllPostWithLimitation(Pagination pagination) {
        return postDAO.getAllPostWithLimitation(pagination);

    }

    public List<PostEntity> getAllApprovedPost(Pagination pagination) {
        return postDAO.getAllApprovedPost(pagination);
    }

    public List<PostEntity> getAllPostOfUser(Pagination pagination, String username) {
        return postDAO.getAllPostOfUser(pagination, username);
    }

    public PostEntity getSinglePost(int id) {
        return postDAO.getSinglePost(id);
    }

    public void changeApprove(int id, boolean approve) {
        postDAO.changeApprove(id, approve);
    }

    public void increaseView(int id) {
        postDAO.increaseView(id);
    }

    public int[] postStatistic() {
        return postDAO.postStatistic();
    }

    public List<PostEntity> searchByTitle(Pagination pagination, String keyword) {
        return postDAO.searchByTitle(pagination, keyword);
    }

    public List<PostEntity> searchByAuthor(Pagination pagination, String keyword) {
        return postDAO.searchByAuthor(pagination, keyword);
    }

    public List<PostEntity> searchUsersPost(Pagination pagination, String username, String keyword) {
        return postDAO.searchUserPost(pagination, username, keyword);
    }

    public Map<String, Object> searchPublicApprovedPosts(Pagination pagination, String keyword) {
        return postDAO.searchPublicApprovedPosts(pagination, keyword);
    }

    public int increaseLike(int id, int value) {
        PostEntity postEntity = postDAO.find(id);
        postEntity.setNoLike(postEntity.getNoLike() + value);
        postDAO.save(postEntity);
        return postEntity.getNoLike();
    }

    public List<PostEntity> getAllPostOfAuthor(Pagination pagination, String username) {
        return postDAO.getAllPostOfAuthor(pagination, username);
    }

    public List<PostEntity> searchAuthorsPost(Pagination pagination, String username, String keyword) {
        return postDAO.searchAuthorPost(pagination, username, keyword);
    }

    public List<PostEntity> findAllPostsByUsernameWithoutLimitation(String username) {
        return postDAO.findAllPostsByUsernameWithoutLimitation(username);
    }
    public PostEntity getPostOfUser(int id, UserEntity user) {
        return postDAO.getPostOfUser(id, user);
    }
}
