package dao;

import entities.AbstractEntity;

import java.util.List;

/**
 * Created by dangdien on 6/8/17.
 */
public interface AbstractDAO<E extends AbstractEntity> {
    public E find(int id);

    public void delete(int id);

    public void save(E e);

    public List<E> findAll();

    public Class getTypeClass();

    public String getTableName();
}