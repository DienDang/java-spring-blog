package dao;

import entities.RoleEntity;
import entities.UserEntity;

import java.util.List;

/**
 * Created by dangdien on 6/8/17.
 */
public interface RoleDAO extends AbstractDAO<RoleEntity>{
    void createRoleForUser(UserEntity userId, String role);
    void updateRoleForUser(UserEntity user, String role);

    List<RoleEntity> listRoleByOrder(String orderBy, String orderMode);

    List<RoleEntity> searchUser(String orderBy, String orderMode, String keyword);
}
