package dao;

import entities.PostEntity;
import entities.UserEntity;
import request.Pagination;

import java.util.List;
import java.util.Map;

/**
 * Created by dangdien on 5/6/17.
 */
public interface PostDAO extends AbstractDAO<PostEntity>{
    public List<PostEntity> getAllPostWithLimitation(Pagination pagination);
    public List<PostEntity> getAllApprovedPost(Pagination pagination);
    public List<PostEntity> getAllPostOfUser(Pagination pagination, String username);
    public PostEntity getSinglePost(int id);
    public void changeApprove(int id, boolean approve);
    public void increaseView(int id);
    public int[] postStatistic();
    List<PostEntity> searchByTitle(Pagination pagination, String keyword);
    List<PostEntity> searchByAuthor(Pagination pagination, String keyword);

    List<PostEntity> searchUserPost(Pagination pagination, String username, String keyword);

    Map<String, Object> searchPublicApprovedPosts(Pagination pagination, String keyword);

    List<PostEntity> getAllPostOfAuthor(Pagination pagination, String username);

    List<PostEntity> searchAuthorPost(Pagination pagination, String username, String keyword);

    List<PostEntity> findAllPostsByUsernameWithoutLimitation(String username);

    PostEntity getPostOfUser(int id, UserEntity user);
}
