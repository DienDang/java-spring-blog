package dao;

import entities.AbstractEntity;
import entities.ConfigurationEntity;

import java.util.List;

/**
 * Created by dangdien on 5/6/17.
 */
public interface ConfigDAO extends AbstractDAO<ConfigurationEntity> {
    public void saveConfig(ConfigurationEntity config);
}
