package dao;

import entities.UserEntity;

/**
 * Created by dangdien on 5/6/17.
 */
public interface UserDAO extends AbstractDAO<UserEntity>{
    UserEntity findUserByUsername(String username);
    UserEntity createUser(String username, String password, String role);
    void checkUsername(String username);
    void checkUpdateUsername(int id, String username);
    void activeUser(int id, boolean status);
}
