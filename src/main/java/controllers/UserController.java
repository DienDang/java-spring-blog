package controllers;


import com.sun.org.apache.regexp.internal.RE;
import entities.UserEntity;
import forms.UserForm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import response.Response;
import services.UserService;

import java.util.*;

/**
 * Created by dangdien on 6/21/17.
 */

@Controller
public class UserController {
    @Autowired
    UserService userService;

    @RequestMapping(value = "/user-info", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody Response userName() {
        org.springframework.security.core.Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        Response response = new Response(Response.ResponseStatus.SUCCESS);
        if(SecurityContextHolder.getContext().getAuthentication() instanceof AnonymousAuthenticationToken) {
            response.setMessage("Not logged in!");
            response.setStatus(Response.ResponseStatus.ERROR);
            return response;
        }
        response.getData().put("user", userService.getUserbyUsername(auth.getName()).getUsername());
        response.getData().put("id", userService.getUserbyUsername(auth.getName()).getId());
        response.getData().put("role", auth.getAuthorities());
        return response;
    }

    @RequestMapping(value = "/admin/dashboard")
    public String adminDashboard() {
        return "admin-dashboard";
    }

    @RequestMapping(value = "/admin/user-list")
    public @ResponseBody List<UserEntity> userList() {
        return userService.findAll();
    }

    @RequestMapping(value = "/admin/user")
    public String user() {
        return "admin-user";
    }

    @RequestMapping(value = "/admin/create-user")
    public @ResponseBody Response createUser(@RequestBody UserForm user) {
        Response response = new Response();
        if(user.getPassword() == null || user.getUsername() == null || user.getRole() == null) {
            response.setStatus(Response.ResponseStatus.ERROR);//check if sent password is null or Role is null, if yes => return status of ERROR
            response.setMessage("BLANK_FIELDS_ERROR");
            return response;
        }
        try {
            userService.createUser(user.getUsername(), user.getPassword(), user.getRole());
            response.setStatus(Response.ResponseStatus.SUCCESS);
        } catch (Exception e) {
            response.setStatus(Response.ResponseStatus.ERROR);
            response.setMessage(e.getMessage());
        }
        return response;
    }

    @RequestMapping(value = "/admin/update-user")
    public @ResponseBody
    Response updateUser(@RequestBody UserForm user) {
        Response response = new Response();

        if(user.getUsername() == null || user.getRole() == null) {
            response.setMessage("BLANK_FIELDS_ERROR"); //check if sent username is null or Role is null, if yes => return status of 0\
            response.setStatus(Response.ResponseStatus.ERROR);
            return response;
        }

        try {
            userService.updateUser(user.getId(), user.getUsername(), user.getPassword(), user.getRole());
            response.setStatus(Response.ResponseStatus.SUCCESS);
        }
        catch (Exception e) {
            response.setMessage(e.getMessage());
            response.setStatus(Response.ResponseStatus.ERROR);
        }
        return response;
    }

    @RequestMapping(value = "/admin/active-user")
    public @ResponseBody Response activeUser(@RequestParam("id") int id, @RequestParam() boolean status) {
        Response response = new Response();
            userService.activeUser(id, status);
            response.setStatus(Response.ResponseStatus.SUCCESS);
        return response;
    }

    @RequestMapping(value = "/user/change-password")
    public @ResponseBody Response changePassword(@RequestParam("id") int id,@RequestParam("oldPassword") String oldPassword, @RequestParam("password") String password) {
        Response response = new Response();
        try {
            userService.changePasswordForUser(id, oldPassword, password);
            response.setStatus(Response.ResponseStatus.SUCCESS);

        }
        catch (Exception e) {
            response.setMessage(e.getMessage());
            response.setStatus(Response.ResponseStatus.ERROR);
        }
        return response;
    }

}
