package controllers;

import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Created by dangdien on 6/19/17.
 */
@Controller
public class ViewsController {

    @RequestMapping(value = {"/home"})
    public String homeView() {
        return "home";
    }

    @RequestMapping(value = "/login")
    public String loginView(){
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        if (!(auth instanceof AnonymousAuthenticationToken)) {
            return "index";
        }
        return "login-form";
    }
}
