package controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.sun.org.apache.regexp.internal.RE;
import dao.UserDAO;
import daoimpls.PostImpls;
import entities.PostEntity;
import entities.UserEntity;
import javafx.geometry.Pos;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpRequest;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.servletapi.SecurityContextHolderAwareRequestWrapper;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import request.Pagination;
import response.Response;
import services.PostService;
import services.UserService;

import javax.servlet.http.HttpServletRequest;
import java.util.*;

/**
 * Created by dangdien on 6/20/17.
 */
@Controller
public class PostController {

    @Autowired
    UserService userService;

    @Autowired
    PostService postService;

    @RequestMapping(value = "/user/write-post")
    public String writePostController() {
        return "write-blog";
    }

    @RequestMapping(value = "/user/save-blog", method = RequestMethod.POST)
    public @ResponseBody
    Response saveBlogPost(@RequestParam("title") String title,
                          @RequestParam("content") String content,
                          @RequestParam("image") String image,
                          @RequestParam("status") boolean status) {
        Response response = new Response();
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();

        if (content == null || title == null) {
            response.setMessage("BLANK_FIELDS_ERROR");
            response.setStatus(Response.ResponseStatus.ERROR);
            return response;
        }
        PostEntity postEntity;
        postEntity = new PostEntity();
        postEntity.setTitle(title);
        postEntity.setContent(content);
        postEntity.setPostTime(new Date());
        postEntity.setNoView(0);
        postEntity.setNoLike(0);
        postEntity.setAuthor(userService.getUserbyUsername(authentication.getName()));
        postEntity.setStatus(status);
        postEntity.setApprove(false);
        postEntity.setApprovedTime(new Date());
        postEntity.setLastUpdate(new Date());
        postEntity.setLastUpdateAuthor(authentication.getName());
        postEntity.setImageLink(image);

        postService.save(postEntity);
        response.setStatus(Response.ResponseStatus.SUCCESS);
        response.setMessage(String.valueOf(postEntity.getId()));
        return response;
    }

    @RequestMapping(value = "/blog-list")
    public @ResponseBody
    Response blogList(@ModelAttribute Pagination pagination
    ) {
        Response response = new Response();
        response.getData().put("object", postService.getAllApprovedPost(pagination));
        response.setStatus(Response.ResponseStatus.SUCCESS);
        return response;
    }

    @RequestMapping(value = "/author/blog-list")
    public @ResponseBody
    Response authorBlogList(@ModelAttribute Pagination pagination, @RequestParam("username") String username
    ) {
        Response response = new Response();
        response.getData().put("object", postService.getAllPostOfAuthor(pagination, username));
        response.setStatus(Response.ResponseStatus.SUCCESS);
        return response;
    }


    @RequestMapping(value = "/user/blog-list")
    public @ResponseBody
    Response userBlogList(@ModelAttribute Pagination pagination,
                          @RequestParam("username") String username) {
        Response response = new Response();
        response.getData().put("object", postService.getAllPostOfUser(pagination, username));
        response.setStatus(Response.ResponseStatus.SUCCESS);
        return response;
    }

    @RequestMapping(value = "/user/dashboard")
    public String userDashboard() {
        return "user-dashboard";
    }

    @RequestMapping(value = "/user/edit-post")
    public String editPost(@RequestParam("id") int id, HttpServletRequest request) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (request.isUserInRole("ROLE_ADMIN")) {
            return "edit-post";
        }
        try {
            PostEntity post = postService.getPostOfUser(id, userService.getUserbyUsername(authentication.getName()));
            if (post != null) {
                return "edit-post";
            }
        } catch (Exception e) {
            return "home";
        }
        return "home";
        //get current user by auth => load all blog => check if blog list contains "id" of this post
    }

    @RequestMapping(value = "/post")
    public @ResponseBody
    Response getSinglePost(@RequestParam("id") int id, HttpServletRequest request) {
        Response response = new Response();
        PostEntity postEntity = postService.getSinglePost(id);
        if (postEntity.isStatus() && postEntity.isApprove()) { //if post is approved and public then everyone can read this post
            response.getData().put("object", postEntity);
            response.setStatus(Response.ResponseStatus.SUCCESS);
            return response;
        }

        Authentication auth = SecurityContextHolder.getContext().getAuthentication();

        UserEntity userEntity = userService.getUserbyUsername(auth.getName()); //get current user
        if (postService.find(id).getAuthor().getId() == userEntity.getId()) {  //check if author id = current user's id
            response.getData().put("object", postEntity);
            response.setStatus(Response.ResponseStatus.SUCCESS);
            return response;                                                 //if status = private => only author and admin can read this post
        }

        if (request.isUserInRole("ROLE_ADMIN")) {
            response.getData().put("object", postEntity);
            response.setStatus(Response.ResponseStatus.SUCCESS);
            return response;
        }

        response.setStatus(Response.ResponseStatus.ERROR);
        response.setMessage("ERROR");
        return response;
    }

    @RequestMapping(value = "/user/update-post", method = RequestMethod.POST)
    public @ResponseBody
    Response updatePost(@RequestBody PostEntity post) {
        Response response = new Response();
        postService.save(post);
        response.setStatus(Response.ResponseStatus.SUCCESS);
        return response;
    }

    @RequestMapping(value = "/user/delete-post", method = RequestMethod.POST)
    public @ResponseBody
    Response deletePost(@RequestParam("id") int id, HttpServletRequest request) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        Response response = new Response();

        if (request.isUserInRole("ROLE_ADMIN")) { //if user is role of ADMIN, then delete
            postService.delete(id);
            response.setStatus(Response.ResponseStatus.SUCCESS);
            return response;
        }

        PostEntity post = postService.getPostOfUser(id, userService.getUserbyUsername(authentication.getName()));
        if (post != null) {
            postService.delete(id);
            response.setStatus(Response.ResponseStatus.SUCCESS);
            return response;
        }
        response.setStatus(Response.ResponseStatus.ERROR);
        response.setMessage("ERROR");
        return response;
    }


    @RequestMapping(value = "/admin/blog-list")
    public @ResponseBody
    Response adminBlogList(@ModelAttribute Pagination pagination) {
        Response response = new Response();
        response.getData().put("object", postService.getAllPostWithLimitation(pagination));
        response.setStatus(Response.ResponseStatus.SUCCESS);
        return response;
    }


    @RequestMapping(value = "/admin/change-approve")
    public @ResponseBody
    Response changeApprove(@RequestParam("postId") int id,
                           @RequestParam("approve") boolean approve) {

        Response response = new Response();
        postService.changeApprove(id, approve);
        response.setStatus(Response.ResponseStatus.SUCCESS);
        return response;
    }

    @RequestMapping(value = "/increase-view")
    public @ResponseBody
    Response increaseView(@RequestParam int id) {
        Response response = new Response();
            postService.increaseView(id);
            response.setStatus(Response.ResponseStatus.SUCCESS);
        return response;
    }

    @RequestMapping(value = "/post-statistic")
    public @ResponseBody
    Response postStatistic() {
        Response response = new Response();
        response.getData().put("approve", postService.postStatistic()[0]);
        response.getData().put("not_approve", postService.postStatistic()[1]);
        return response;
    }

    // search all posts of all user (ROLE = admin)
    @RequestMapping(value = "/admin/search")
    public @ResponseBody
    Response searchByTitle(@ModelAttribute Pagination pagination,
                           @RequestParam("keyword") String keyword,
                           @RequestParam("option") String option) {
        Response response = new Response();
        if (option.equals("title")) {
            response.getData().put("object", postService.searchByTitle(pagination, keyword));
            response.setStatus(Response.ResponseStatus.SUCCESS);
            return response;
        } else {
            response.getData().put("object", postService.searchByAuthor(pagination, keyword));
            response.setStatus(Response.ResponseStatus.SUCCESS);
            return response;
        }
    }

    // search all posts of this user (ROLE = user)
    @RequestMapping(value = "/user/search")
    public @ResponseBody
    Response userSearch(@ModelAttribute Pagination pagination,
                        @RequestParam("username") String username,
                        @RequestParam("keyword") String keyword) {
        Response response = new Response();
        response.getData().put("object", postService.searchUsersPost(pagination, username, keyword));
        response.setStatus(Response.ResponseStatus.SUCCESS);
        return response;
    }

    //search all public approved post of user (ROLE = guest)
    @RequestMapping(value = "/author/search")
    public @ResponseBody
    Response authorSearch(@ModelAttribute Pagination pagination,
                          @RequestParam("username") String username,
                          @RequestParam("keyword") String keyword) {
        Response response = new Response();
        response.getData().put("object", postService.searchAuthorsPost(pagination, username, keyword));
        response.setStatus(Response.ResponseStatus.SUCCESS);
        return response;
    }

    // search all approved and public posts (ROLE = guest)
    @RequestMapping(value = "/search")
    public @ResponseBody
    Response search(@ModelAttribute Pagination pagination,
                    @RequestParam("keyword") String keyword) {
        Response response = new Response();
        response.setData(postService.searchPublicApprovedPosts(pagination, keyword));
        response.setStatus(Response.ResponseStatus.SUCCESS);
        return response;
    }

    @RequestMapping(value = "/increase-like")
    public @ResponseBody
    Response increaseLike(@RequestParam("id") int id,
                          @RequestParam("value") int value) {
        Response response = new Response();
        response.getData().put("object", postService.increaseLike(id, value));
        response.setStatus(Response.ResponseStatus.SUCCESS);
        return response;
    }
}
