package controllers;

import dao.ConfigDAO;
import dao.RoleDAO;
import dao.UserDAO;
import daoimpls.ConfigImpls;
import entities.ConfigurationEntity;
import entities.RoleEntity;
import entities.UserEntity;
import forms.ConfigForm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import response.Response;
import services.ConfigService;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by dangdien on 5/6/17.
 */
@Controller
public class ConfigController {
    @Autowired
    ConfigService configService;

    @RequestMapping("/getconfig")
    public @ResponseBody Response getAppConfig() {
        Response response = new Response(Response.ResponseStatus.SUCCESS);
        response.getData().put("object", configService.findAll());
        return response;
    }

    @RequestMapping("/admin/config")
    public String adminConfig() {
        return "admin-config";
    }

    @RequestMapping(value = "/admin/update-config")
    public @ResponseBody
    Response updateConfig(@RequestBody ConfigForm configList) {
        Response response = new Response(Response.ResponseStatus.SUCCESS);
        configService.save(configList.getConfig());
        return response;
    }

}
