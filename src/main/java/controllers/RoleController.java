package controllers;

import entities.RoleEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import response.Response;
import services.RoleService;

import java.util.List;

/**
 * Created by dangdien on 6/29/17.
 */
@Controller
public class RoleController {

    @Autowired
    RoleService roleService;

    @RequestMapping(value = "/admin/roles")
    public @ResponseBody
    Response getRoles(@RequestParam("orderBy") String orderBy, @RequestParam("orderMode") String orderMode) {
        Response response = new Response(Response.ResponseStatus.SUCCESS);
        response.getData().put("object",roleService.listRoleByOrder(orderBy, orderMode));
        return response;
    }

    @RequestMapping(value = "/admin/search-user")
    public @ResponseBody Response searchRole(@RequestParam("orderBy") String orderBy, @RequestParam("orderMode") String orderMode, @RequestParam("keyword") String keyword) {
        Response response = new Response(Response.ResponseStatus.SUCCESS);
        response.getData().put("object",roleService.searchUser(orderBy, orderMode, keyword));
        return response;
    }
}
