/**
 * Created by dangdien on 6/29/17.
 */
demoApp.controller('userCtrl', ['$scope', '$http', '$uibModal', '$filter', function ($scope, $http, $uibModal, $filter) {
    $scope.userRoleList = [];
    $scope.alerts = [];
    $scope.orderBy = "username";
    $scope.orderMode = "DESC";
    $scope.orderModeBool = true;

    $scope.changeOrderMode = function (value) {
        if($scope.orderBy == value) {
            $scope.orderModeBool = !$scope.orderModeBool;
        }
        if($scope.orderModeBool == true) {$scope.orderMode = "DESC"}
        else {$scope.orderMode = "ASC"}
    };

    $scope.loadData = function() {
        // $http.get("/admin/roles").then(function (response) {
        //     $scope.userRoleList = response.data;
        // });
        if(!$scope.keyword) {
            $http({
                method: 'GET',
                url: "/admin/roles",
                params: {
                    orderBy: $scope.orderBy,
                    orderMode: $scope.orderMode
                }
            }).then(function (response) {
                $scope.userRoleList = response.data.data.object;
            })
        }
        else {
            $http({
                method: 'GET',
                url: "/admin/search-user",
                params: {
                    orderBy: $scope.orderBy,
                    orderMode: $scope.orderMode,
                    keyword: "%"+$scope.keyword+"%"
                }
            }).then(function (response) {
                $scope.userRoleList = response.data.data.object;
            })
        }
    };
    $scope.search = function () {
        $scope.loadData();
    };
    $scope.loadData();
    $scope.closeAlert = function(index) {
        $scope.alerts.splice(index, 1);
    };

    $scope.openUpdate = function (userRole) {
        var modalUpdate = $uibModal.open({
            animation: true,
            component: 'updateUser',
            backdrop: false,
            resolve: {
                userRole: userRole
            }
        });

        modalUpdate.result.then(function (response) {
                $scope.loadData();
        })
       //unhandled reject
    };

    $scope.openCreate = function () {

        var modalCreate = $uibModal.open({
            animation: true,
            component: 'updateUser',
            backdrop: false
        });

        modalCreate.result.then(function (response) {
            $scope.loadData();
        })

        //unhandled reject
    }

    $scope.activeUser = function (user) {
        console.log("called");
        $http({
            method: 'POST',
            url: "/admin/active-user",
            params: {
                id: user.id,
                status: user.enabled ? false : true
            }
        }).then(function (response) {
                $scope.loadData();
        });
    };

    $scope.$watch('orderBy', function (newValue, oldValue) {
        $scope.loadData();
    });
    $scope.$watch('orderMode', function (a,b) {
        $scope.loadData();
    })


}]);