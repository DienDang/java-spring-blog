/**
 * Created by dangdien on 6/29/17.
 */
demoApp.directive('updateUser', function () {
    return {
        templateUrl: "/directives/create-user.html",
        restrict: 'E',
        scope: {
            resolve: '<',
            close: '&',
            dismiss: '&'
        },
        controller: ['$scope', '$http', '$filter', function ($scope, $http, $filter) {
            $scope.userRole = $scope.resolve.userRole;
            $scope.alerts = [];
            $scope.selectedRole = "ROLE_USER";

            $scope.closeAlert = function(index) {
                $scope.alerts.splice(index, 1);
            };


            $scope.cancel = function () {
                $scope.close();
            };

            $scope.changeRole = function (value) {
                $scope.selectedRole = value;
            };

            if($scope.userRole) {
                $scope.user = $scope.userRole.user;
                $scope.iUsername = $scope.user.username;
                $scope.selectedRole = $scope.userRole.role;
            }

            $scope.processUser = function (userId) {
                if(!$scope.changePassword) {
                    if(!$scope.updateForm.usernameInput.$valid) {
                        $scope.alerts = [];
                        $scope.alerts.push({type: 'danger', msg: $filter('translate')('BLANK_FIELDS_ERROR')})
                    }
                    else {
                        if (!$scope.userRole) {
                            $scope.createUser();
                        }
                        else {
                            $scope.updateUser(userId);
                        }
                    }
                }
                else {
                    if (!$scope.updateForm.usernameInput.$valid || !$scope.updateForm.passwordInput.$valid || !$scope.updateForm.rePasswordInput.$valid) {
                        $scope.alerts = [];
                        $scope.alerts.push({type: 'danger', msg: $filter('translate')('BLANK_FIELDS_ERROR')})
                    }
                    else {
                        if (!$scope.userRole) {
                            $scope.createUser();
                        }
                        else {
                            $scope.updateUser(userId);
                        }
                    }
                }
            };

            $scope.createUser = function () {
                if($scope.iPassword !== $scope.iRePassword){
                    $scope.alerts = [];
                    $scope.alerts.push({type: 'danger', msg: $filter('translate')('PASSWORD_MATCH_ERROR')})
                }
                else {
                    $http.post("/admin/create-user", {
                        username: $scope.iUsername,
                        password: $scope.iPassword,
                        role: $scope.selectedRole
                    }).then(function (response) {
                        if (response.data.status == "ERROR") {
                            $scope.alerts = [];
                            $scope.alerts.push({type: 'danger', msg: $filter('translate')(response.data.message)})
                        }
                        else {
                            $scope.alerts = [];
                            $scope.alerts.push({type: 'success', msg: $filter('translate')('SUCCESS')})
                            $scope.close();
                        }
                    })
                }
            };

            $scope.updateUser = function (userId) {
                if($scope.iPassword !== $scope.iRePassword){
                    $scope.alerts = [];
                    $scope.alerts.push({type: 'danger', msg: $filter('translate')('PASSWORD_MATCH_ERROR')})
                }
                else {
                    $http.post("/admin/update-user", {
                        username: $scope.iUsername,
                        password: $scope.iPassword,
                        role: $scope.selectedRole,
                        id: userId
                    }).then(function (response) {
                        if (response.data.status == "ERROR") {
                            $scope.alerts = [];
                            $scope.alerts.push({type: 'danger', msg: $filter('translate')(response.data.message)})
                        }
                        else {
                            $scope.alerts = [];
                            $scope.alerts.push({type: 'success', msg: $filter('translate')('SUCCESS')})
                        }
                    })
                }
            };
            }]
        }
});