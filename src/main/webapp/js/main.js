/**
 * Created by dangdien on 5/5/17.
 */
var demoApp = angular.module("demoApp", ["ngRoute", "textAngular", "ngSanitize", "ui.bootstrap", "mwl.confirm", "ui.router", 'pascalprecht.translate', 'ngCookies']);

demoApp.factory('userInfo', ['$http', function ($http) {
    var info = {};
    return $http({
        method: 'GET',
        url: "/user-info"
    }).then(function (response) {
        if (response.data.status === "SUCCESS") {
            info.authenticated = true;
            info.username = response.data.data.user;
            info.userId = response.data.data.id;
            info.role = response.data.data.role;
            return info;
        }
    })
}]);


demoApp.controller("mainCtrl", ['$scope', '$http', "$route", "$window", 'userInfo', '$translate', '$filter', '$cookies', '$uibModal', function ($scope, $http, $route, $window, userInfo, $translate, $filter, $cookies, $uibModal) {
    $scope.articles = [];
    $scope.appConfigs = null;
    $scope.credentials = {};
    $scope.authenticated = false;
    $scope.orderBy = "post_time";
    $scope.orderMode = "DESC";
    $scope.pageOffset = 0;
    $scope.blogList = [];
    $scope.page = 1;
    $scope.curentLang = 'vi';
    $scope.searchSize = 0;
    $scope.searchShow = false;



    $scope.reload = function () {
        $window.location.reload();
    };


    $translate.use($cookies.get('langKey'));

    $scope.changeLanguage = function (langKey) {
        $scope.curentLang = langKey;
        $cookies.put('langKey', langKey);
        $translate.use(langKey);
    };

    userInfo.then(function (info) {
        if(info) {
            $scope.authenticated = info.authenticated;
            if ($scope.authenticated) {
                $scope.username = info.username;
                $scope.userId = info.userId;
                $scope.role = info.role;
                $scope.checkAdmin = function () {
                    if ($scope.role[0].authority == 'ROLE_ADMIN') {
                        return true;
                    }
                    return false;
                };
            }
        }
    });

    $scope.search = function () {
        $scope.page = 1;
        $scope.pageOffset = 0;
        $scope.mainFunc();
    };


    $scope.mainFunc = function () {
        $http({
            method: 'GET',
            url: '/getconfig'
        }).then(function (response) {
            $scope.pageConfig = response.data.data.object;
            $scope.getBlogList = function (offset) {
                if (!$scope.keyword) {
                    return $http({
                        method: 'GET',
                        url: '/blog-list',
                        params: {
                            limit: $scope.pageConfig[2].value,
                            orderBy: $scope.orderBy,
                            orderMode: $scope.orderMode,
                            offset: offset
                        }
                    }).then(function (response) {
                        if(response.data.status === "SUCCESS") {
                            return response.data.data.object;
                        }
                        else {
                            console.log(response.data.message);
                        }
                    });
                }
                else {
                    return $http({
                        method: 'GET',
                        url: '/search', //search all approved and public posts for guests
                        params: {
                            limit: $scope.pageConfig[2].value,
                            orderBy: $scope.orderBy,
                            orderMode: $scope.orderMode,
                            offset: offset,
                            keyword: "%" + $scope.keyword + "%"
                        }
                    }).then(function (response) {
                        if(response.data.status === "SUCCESS") {
                            $scope.searchSize = response.data.data.count; //server is wrong
                            return response.data.data.list;
                        }
                        else {
                            console.log(response.data.message);
                        }
                    });
                }
            };
            //PAGINATION
            $scope.getBlogList($scope.pageOffset).then(function (data) {
                $scope.blogList = data;
                $scope.getNextPage = function () {
                    $scope.pageOffsetNext = $scope.pageOffset + parseInt($scope.pageConfig[2].value);
                    $scope.getBlogList($scope.pageOffsetNext).then(function (nextData) {
                        $scope.nextList = nextData;

                        if ($scope.nextList.length) {
                            $scope.nextPage = function () {
                                $scope.blogList = $scope.nextList;
                                $scope.pageOffset += parseInt($scope.pageConfig[2].value);
                                $scope.getNextPage();
                                $scope.page++;
                            };
                        }
                    });
                };

                $scope.getNextPage();
                $scope.prevPage = function () {
                    $scope.page--;
                    $scope.pageOffset -= parseInt($scope.pageConfig[2].value);
                    $scope.getBlogList($scope.pageOffset).then(function (data) {
                        $scope.blogList = data;
                    });
                    $scope.getNextPage();
                }

            });
        });
    };
    $scope.mainFunc();

    $scope.changePassword = function () {
        var modalCreate = $uibModal.open({
            animation: true,
            component: 'changePassword',
            backdrop: true,
            resolve: {
                userId: $scope.userId
            }
        });
    }
}]);


//Upload file directive
demoApp.directive('fileModel', ['$parse', function ($parse) {
    return {
        restrict: 'A',
        link: function (scope, element, attrs) {
            var model = $parse(attrs.fileModel);
            var modelSetter = model.assign;

            element.bind('change', function () {
                scope.$apply(function () {
                    modelSetter(scope, element[0].files[0]);
                });
            });
        }
    };
}]);
