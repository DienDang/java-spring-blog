/**
 * Created by dangdien on 6/22/17.
 */
demoApp.controller("writeBlogCtrl", ['$scope', '$http', 'userInfo', '$location', '$filter', function ($scope, $http, userInfo, $location,$filter) {
    $scope.privacyMode = 0;
    $scope.alerts = [];
    userInfo.then(function (info) {
        $scope.username = info.username;
    });

    $scope.changePrivacy = function(value) {
        $scope.privacyMode = value;
    };

    $scope.closeAlert = function(index) {
        $scope.alerts.splice(index, 1);
    };

    $scope.processArticle = function () {
        if(!$scope.title) {
            $scope.alerts = [];
            $scope.alerts.push({type: 'danger', msg: $filter('translate')('TITLE_NULL_ERROR')});
            return;
        }
        else if (!$scope.htmlVariable) {
            $scope.alerts = [];
            $scope.alerts.push({type: 'danger', msg: $filter('translate')('CONTENT_NULL_ERROR')});
            return;
        }
        else {
            $http({
                method: 'POST',
                url: "/user/save-blog",
                params: {
                    title: $scope.title,
                    content: $scope.htmlVariable,
                    image: $scope.imageLink ? $scope.imageLink : "",
                    status: $scope.privacyMode
                }
            }).then(function (response) {
                if(response.data.status == "SUCCESS") {
                    $location.path("/view-single-post/" + response.data.message)
                }
            })
        }
    }
}]);