<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>
<%--
  Created by IntelliJ IDEA.
  User: dangdien
  Date: 6/19/17
  Time: 4:55 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<div class="container">
    <div class="row">
        <div class="col-sm-6 col-md-4 col-md-offset-4">
            <h1 class="text-center login-title">{{'SIGNIN_TOCONTINUE' | translate}}</h1>
            <div class="account-wall">
                <img class="profile-img" src="https://lh5.googleusercontent.com/-b0-k99FZlyE/AAAAAAAAAAI/AAAAAAAAAAA/eu7opA4byxI/photo.jpg?sz=120"
                     alt="">
                <form action="<s:url value="/login"/>" method="post" class="form-signin">
                    <input type="text" class="form-control" placeholder="{{'USERNAME' | translate}}" id="username" name="username" ng-model="credentials.username" required autofocus>
                    <input type="password" class="form-control" placeholder="{{'PASSWORD' | translate}}" id="password" name="password" ng-model="credentials.password" required>
                    <button class="btn btn-lg btn-primary btn-block" type="submit">
                        {{'SIGNIN' | translate}}</button>
                </form>
            </div>
        </div>
    </div>
</div>