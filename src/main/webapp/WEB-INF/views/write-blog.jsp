<%--
  Created by IntelliJ IDEA.
  User: dangdien
  Date: 6/20/17
  Time: 3:49 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<div class="container mt40" ng-controller="writeBlogCtrl">
    <div uib-alert ng-repeat="alert in alerts" ng-class="'alert-' + (alert.type || 'warning')" close="closeAlert($index)">{{alert.msg}}</div>
    <div>
        <label for="title">{{'TITLE' | translate}} :</label>
        <input type="text" class="form-control" id="title" ng-model="title">
    </div>

    <div class="pt10">
        <label for="image">{{'IMAGE' | translate}} : </label>
        <input type="text" class="form-control" id="image" ng-model="imageLink">
    </div>
    <div class="pt10">
        <label>{{'STATUS' | translate}} : </label>
        <div class="dropdown">
            <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">{{privacyMode == 0 ? 'Private' : 'Public'}}
                <span class="caret"></span></button>
            <ul class="dropdown-menu">
                <li><a href="" ng-click="changePrivacy(false)">Private</a></li>
                <li><a href="" ng-click="changePrivacy(true)">Public</a></li>
            </ul>
        </div>
        <%--<select type="text" class="form-control privacy-select" id="mode" ng-model="privacyMode">--%>
            <%--<option value="0" selected>Private</option>--%>
            <%--<option value="1">Public</option>--%>
        <%--</select>--%>
    </div>

    <div class="mt40">
        <div text-angular ng-model="htmlVariable"></div>
    </div>
    <div class="text-right mt40">
        <button ng-click="processArticle()" type="button" class="btn btn-success write-button"><a href="">{{'POST' | translate}}</a></button>
    </div>
</div>


