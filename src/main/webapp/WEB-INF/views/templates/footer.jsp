<%--
  Created by IntelliJ IDEA.
  User: dangdien
  Date: 6/20/17
  Time: 3:44 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<div class="container">
<footer>
    <div class="row">
        <div class="col-lg-12">
            <p>Copyright &copy; Dang Dien 2017</p>
        </div>
        <!-- /.col-lg-12 -->
    </div>

    <!-- /.row -->
</footer>
</div>
<!-- jQuery -->
<script src="../vendors/jquery.js"></script>

<!-- Bootstrap Core JavaScript -->
<script src="/vendors/bootstrap.min.js"></script>
<script src="/vendors/angular.min.js"></script>
<script src="/vendors/angular-route.js"></script>
<script src='/vendors/textAngular/textAngular-rangy.min.js'></script>
<script src='/vendors/textAngular/textAngular-sanitize.min.js'></script>
<script src="/vendors/textAngular/textAngular.min.js"></script>
<script src="/vendors/ui-bootstrap.js"></script>
<script src="/vendors/angular-bootstrap-confirm.js"></script>
<script src="//unpkg.com/angular-ui-router/release/angular-ui-router.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/angular-translate/2.15.2/angular-translate.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.4/angular-cookies.js"></script>
<script src="/js/main.js"></script>
<script src="/js/writeBlog.js"></script>
<script src="/js/user-dashboard.js"></script>
<script src="/js/edit-post.js"></script>
<script src="/js/single-post.js"></script>
<script src="/js/admin-dashboard.js"></script>
<script src="/js/admin-config.js"></script>
<script src="/js/admin-user.js"></script>
<script src="/js/update-user.js"></script>
<script src="/js/config/angular-config.js"></script>
<script src="/js/config/i18n.js"></script>
<script src="/js/change-password.js"></script>
<script src="/js/user-posts.js"></script>
</body>

</html>


