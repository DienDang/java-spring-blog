<%--
  Created by IntelliJ IDEA.
  User: dangdien
  Date: 6/19/17
  Time: 3:08 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
    <div class="container" ng-cloak ng-init="selectMenu = 1">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#">{{appConfigs.web_Title}}</a>
        </div>
        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse clearfix" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav left-menu">
                <li>
                    <a class="navbar-brand">{{pageConfig[0].value}}</a>
                </li>
                <li>
                    <a href="#!/" ng-click="reload(); selectMenu = 1" ng-class="{'selected-menu' : selectMenu == 1}">{{'HOMEPAGE' | translate}}</a>
                </li>
                <li ng-show="authenticated">
                    <a href="#!write-post" ng-click="selectMenu = 2" ng-class="{'selected-menu' : selectMenu == 2}">{{'WRITE_BLOG' | translate}}</a>
                </li>
                <li ng-show="authenticated">
                    <a href="#!user-dashboard" ng-click="selectMenu = 3" ng-class="{'selected-menu' : selectMenu == 3}">{{'BLOG_MNG' | translate}}</a>
                </li>
                <li ng-show="checkAdmin()">
                    <a href="#!admin/dashboard" ng-click="selectMenu = 4" ng-class="{'selected-menu' : selectMenu == 4}">{{'ADMIN' | translate}}</a>
                </li>
            </ul>
            <ul class="nav navbar-nav right-menu">
                <li class="dropdown" ng-show="authenticated">
                    <a href="" ng-cloak class="dropdown-toggle" data-toggle="dropdown">{{curentLang == 'vi' ? 'Tiếng Việt' : 'English'}} <span class="caret"></span></a>
                    <ul class="dropdown-menu" role="menu">
                        <li><a href="" ng-click="changeLanguage('en')">{{'ENGLISH' | translate}}</a></li>
                        <li><a href="" ng-click="changeLanguage('vi')">{{'VIETNAM' | translate}}</a></li>
                    </ul>
                </li>
                <%--<li>--%>
                    <%--<a href="" ng-click="changeLanguage('en')" >{{'ENGLISH' | translate}}</a>--%>
                <%--</li>--%>
                <%--<li>--%>
                    <%--<a href="" ng-click="changeLanguage('vi')">{{'VIETNAM' | translate}}</a>--%>
                <%--</li>--%>
                <li ng-show="!authenticated">
                    <a href="/#!login" >{{'LOGIN' | translate}}</a>
                </li>
                <li class="dropdown" ng-show="authenticated">
                    <a href="" ng-cloak class="dropdown-toggle" data-toggle="dropdown">{{'HELLO' | translate}} {{username}} <span class="caret"></span></a>
                    <ul class="dropdown-menu" role="menu">
                        <li><a href="" ng-click="changePassword()">{{'CHANGE_PASSWORD' | translate}}</a></li>
                    </ul>
                </li>
                <%--<li ng-show="authenticated">--%>
                    <%--<a ng-cloak>{{'HELLO' | translate}} {{username}}</a>--%>
                <%--</li>--%>
                <li ng-show="authenticated">
                    <a href="/logout">Logout</a>
                </li>
            </ul>
        </div>
        <!-- /.navbar-collapse -->
    </div>
    <!-- /.container -->
</nav>