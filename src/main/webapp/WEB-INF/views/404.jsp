<%--
  Created by IntelliJ IDEA.
  User: dangdien
  Date: 7/5/17
  Time: 5:19 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <link href="/css/blog-home.css" rel="stylesheet">
    <link rel="stylesheet" href="http://netdna.bootstrapcdn.com/bootstrap/3.1.0/css/bootstrap.min.css">
</head>
<body>
<div class="container">
    <div class="row">
        <div class="error-template">
            <h1>Oops!</h1>
            <h2>404 Not Found</h2>
            <div class="error-details">
                Sorry, an error has occured, Requested page not found!<br>
                <?php echo CHtml::encode($message); ?>
            </div>
            <div class="error-actions">
                <a href="/" class="btn btn-primary">
                    <i class="icon-home icon-white"></i> Take Me Home </a>
                <a href="mailto:me@null-byte.info" class="btn btn-default">
                    <i class="icon-envelope"></i> Contact Support </a>
            </div>
        </div>
    </div>
</div>
</body>
</html>