<%--
  Created by IntelliJ IDEA.
  User: dangdien
  Date: 6/29/17
  Time: 9:05 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<div id="wrapper" ng-init="open = false" ng-class="{'toggled': open}" ng-controller="userCtrl">
    <div class="overlay" ng-class="{'opened' :open, 'closed': !open}"></div>

    <!-- Sidebar -->
    <nav class="navbar navbar-inverse navbar-fixed-top" id="sidebar-wrapper" role="navigation">
        <ul class="nav sidebar-nav">
            <li class="sidebar-brand">
                <a href="#/test">
                    Brand
                </a>
            </li>
            <li>
                <a href="#!admin/dashboard">{{'BLOG_MNG' | translate}}</a>
            </li>
            <li>
                <a href="#!/admin/user">{{'AUTHORS' | translate}}</a>
            </li>
            <li>
                <a href="#!admin/config">{{'CONFIG' | translate}}</a>
            </li>
        </ul>
    </nav>
    <!-- /#sidebar-wrapper -->

    <!-- Page Content -->
    <div id="page-content-wrapper">
        <button type="button" class="hamburger" ng-class="{'is-open' :open, 'is-closed': !open}" ng-click="open = !open"
                data-toggle="offcanvas">
            <span class="hamb-top"></span>
            <span class="hamb-middle"></span>
            <span class="hamb-bottom"></span>
        </button>
        <div class="container">
            <div class="row">
                <div uib-alert ng-repeat="alert in alerts" ng-class="'alert-' + (alert.type || 'warning')" close="closeAlert($index)">{{alert.msg}}</div>
                <form>
                <label>{{'FILTER' | translate}} :</label>
                <input type="text" class="form-control" style="width: 200px; display: inline-block" ng-model="keyword">
                <button class="btn btn-default" ng-click="search()" type="submit">{{'FILTER' | translate}}</button>
                <button class="btn btn-default" ng-click="keyword = '';search()" ng-show="keyword">Reset</button>
                </form>
                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th><a href="" ng-click="orderBy = 'username'; changeOrderMode('username')">{{'USERNAME' | translate}}</a></th>
                        <th><a href="" ng-click="orderBy = 'role'; changeOrderMode('role')">{{'ROLE' | translate}}</a></th>
                        <th><a href="" ng-click="orderBy = 'enabled'; changeOrderMode('enabled')">{{'ACTIVATE' | translate}}</a></th>
                        <th><a href="" ng-click="openCreate()" title="Add"><span class="glyphicon glyphicon-plus-sign"></span></a></th>
                    </tr>
                    </thead>

                    <tbody>
                        <tr ng-repeat="userRole in userRoleList">
                            <td>{{userRole.user.username}}</td>
                            <td>{{userRole.role == 'ROLE_ADMIN' ? 'ADMIN_ROLE' : 'AUTHOR_ROLE' | translate}}</td>
                            <td><a href="" ng-click="activeUser(userRole.user)" title="{{userRole.user.enabled ? 'Inactive' : 'Active'}}">
                                <span ng-class="{'glyphicon glyphicon-remove': userRole.user.enabled, 'glyphicon glyphicon-ok': !userRole.user.enabled}"></span>
                            </a></td>
                            <td><a href="" ng-click="openUpdate(userRole)" title="Edit"><span class="glyphicon glyphicon-pencil"></span></a></td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
