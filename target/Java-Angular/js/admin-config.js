/**
 * Created by dangdien on 6/28/17.
 */
demoApp.controller("configController", ['$scope', '$http', '$filter', function ($scope, $http, $filter) {
    $scope.pageConfig = [];
    $scope.alerts = [];
    $scope.closeAlert = function(index) {
        $scope.alerts.splice(index, 1);
    };

    $http.get("/getconfig").then(function (response) {
        $scope.pageConfig = response.data.data.object;
        $scope.postNumber = parseInt($scope.pageConfig[2].value);
        $scope.blogTitle = $scope.pageConfig[0].value;
        $scope.dateFormat = $scope.pageConfig[1].value;
        $scope.data = {
            availableOptions: [
                {id: '1', format: 'hh:MM:ss dd/MM/yyyy'},
                {id: '2', format: 'dd/MM/yyyy hh:MM:ss'},
                {id: '3', format: 'MM/dd/yyyy hh:MM:ss'},
                {id: '4', format: 'yyyy/dd/MM hh:MM:ss'}
            ],
            selectedOption: {id: '1', format: $scope.pageConfig[1].value} //This sets the default value of the select in the ui
        };
    });

    $scope.changeDateFormat = function (value) {
        $scope.dateFormat = value;
    };

    $scope.updateConfig = function () {
        if($scope.postNumber == undefined) {
            $scope.alerts = [];
            $scope.alerts.push({type: 'danger', msg: $filter('translate')('POST_NUMBER_INVALID')});
            return;
        }
        $http.post("/admin/update-config", {
            config: [{
                name: "title",
                value: $scope.blogTitle
            },
                {
                    name: "date_format",
                    value: $scope.dateFormat
                }, {
                    name: "number_post_view",
                    value: $scope.postNumber
                }]
        }).then(function (response) {
            if(response.data.status === "SUCCESS") {
                $scope.alerts = [];
                $scope.alerts.push({type: 'success', msg: $filter('translate')('UPDATE_CONFIG_SUCCESS')});
            }
            else {
                $scope.alerts = [];
                $scope.alerts.push({type: 'success', msg: $filter('translate')(response.data.message)});
            }
        });
    }


}]);