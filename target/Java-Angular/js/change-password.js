/**
 * Created by dangdien on 7/4/17.
 */
demoApp.directive('changePassword', function () {
    return {
        templateUrl: "/directives/change-password.html",
        restrict: 'E',
        scope: {
            resolve: '<',
            close: '&',
            dismiss: '&'
        },
        controller: ['$scope', '$http','$filter', function ($scope, $http, $filter) {
            $scope.userId = $scope.resolve.userId;
            $scope.alerts = [];
            $scope.changePassword = function () {
                if(!$scope.iOPassword) {
                    $scope.alerts = [];
                    $scope.alerts.push({type: 'danger', msg: $filter('translate')('BLANK_FIELDS_ERROR')});
                }
                else if($scope.iPassword !== $scope.iRePassword) {
                    $scope.alerts = [];
                    $scope.alerts.push({type: 'danger', msg: $filter('translate')('PASSWORD_MATCH_ERROR')});
                }
                else if($scope.iPassword === undefined) {
                    $scope.alerts = [];
                    $scope.alerts.push({type: 'danger', msg: $filter('translate')('BLANK_FIELDS_ERROR')});
                }
                else {
                    $http({
                        method: 'POST',
                        url: "/user/change-password",
                        params: {
                            id: $scope.userId,
                            oldPassword: $scope.iOPassword,
                            password: $scope.iPassword
                        }
                    }).then(function (response) {
                        console.log(response);
                        if(response.data.status == "SUCCESS") {
                            $scope.alerts = [];
                            $scope.alerts.push({type: 'success', msg: $filter('translate')('CHANGE_PASSWORD_SUCCESS')});
                        }
                        else {
                            $scope.alerts = [];
                            $scope.alerts.push({type: 'danger', msg: $filter('translate')(response.data.message)});
                        }
                    });
                }
            };

            $scope.cancel = function () {
                $scope.close()
            };

            $scope.closeAlert = function(index) {
                $scope.alerts.splice(index, 1);
            };
        }]
    }
});