/**
 * Created by dangdien on 6/28/17.
 */
demoApp.controller("adminDashboard", ['$scope', '$http', '$filter', function ($scope, $http, $filter) {
    $scope.pageOffset = 0;
    $scope.blogList = [];
    $scope.nextList = [];
    $scope.orderBy = "post_time";
    $scope.orderMode = "DESC";
    $scope.orderModeBool = true;
    $scope.alerts = [];
    $scope.page = 1;
    $scope.searchOption = "title";
    $scope.keyword = "";

    $scope.changeOrderMode = function (value) {
        if ($scope.orderBy == value) {
            $scope.orderModeBool = !$scope.orderModeBool;
        }
        if ($scope.orderModeBool == true) {
            $scope.orderMode = "DESC"
        }
        else {
            $scope.orderMode = "ASC"
        }
    };

    $scope.closeAlert = function (index) {
        $scope.alerts.splice(index, 1);
    };

    $scope.changeMode = function (value) {
        $scope.searchOption = value;
    };

    $scope.getStatistics = function () {
        $http.get("/post-statistic").then(function (response) {
            $scope.approved = response.data.data.approve;
            $scope.notApproved = response.data.data.not_approve;
        });
    };

    $scope.getStatistics();

    $scope.deletePost = function (id) {
        $http({
            method: 'POST',
            url: "/user/delete-post",
            params: {id: id}
        }).then(function (response) {
            if (response.data.status === "SUCCESS") {
                $scope.alerts = [];
                $scope.alerts.push({type: 'success', msg: $filter('translate')('DELETE_POST_SUCCESS')});
                $scope.mainFunc();
            }
            else {
                $scope.alerts = [];
                $scope.alerts.push({type: 'danger', msg: $filter('translate')(response.data.message)});
            }
        });
    };

    $scope.changeApprove = function (post) {
        if(!post.status) {
            return;
        }
        $http({
            method: 'GET',
            url: '/admin/change-approve',
            params: {
                postId: post.id,
                approve: !post.approve
            }
        }).then(function (response) {
            if (response.data.status === "SUCCESS") {
                $scope.getStatistics();
                $scope.mainFunc();
            }
        })
    };

    $scope.search = function () {
        $scope.mainFunc();
    };


    $scope.mainFunc = function () {

        $http.get('/getconfig').then(function (config) {
            $scope.pageConfig = config.data.data.object;
            $scope.pageConfig[2].value = 15;

            $scope.getBlogList = function (offset) {
                if (!$scope.keyword || !$scope.searchOption) {
                    return $http({
                        method: 'GET',
                        url: '/admin/blog-list',
                        params: {
                            limit: $scope.pageConfig[2].value,
                            orderBy: $scope.orderBy,
                            orderMode: $scope.orderMode,
                            offset: offset
                        }
                    }).then(function (response) {
                        return response.data.data.object;
                    });
                    //get user blog list
                }
                else {
                    return $http({
                        method: 'GET',
                        url: "/admin/search",
                        params: {
                            limit: $scope.pageConfig[2].value,
                            orderBy: $scope.orderBy,
                            orderMode: $scope.orderMode,
                            offset: offset,
                            keyword: "%" + $scope.keyword + "%",
                            option: $scope.searchOption
                        }
                    }).then(function (response) {
                        return response.data.data.object;
                    });
                }
            };
            //get user info

            //PAGINATION
            $scope.getBlogList($scope.pageOffset).then(function (data) {
                $scope.blogList = data;
                $scope.getNextPage = function () {
                    $scope.pageOffsetNext = $scope.pageOffset + parseInt($scope.pageConfig[2].value);
                    $scope.getBlogList($scope.pageOffsetNext).then(function (nextData) {
                        $scope.nextList = nextData;

                        if ($scope.nextList.length) {
                            $scope.nextPage = function () {
                                $scope.blogList = $scope.nextList;
                                $scope.pageOffset += parseInt($scope.pageConfig[2].value);
                                $scope.getNextPage();
                                $scope.page++;
                            };
                        }
                    });
                };

                $scope.prevPage = function () {
                    $scope.page--;
                    $scope.pageOffset -= parseInt($scope.pageConfig[2].value);
                    $scope.getBlogList($scope.pageOffset).then(function (data) {
                        $scope.blogList = data;
                    });
                    $scope.getNextPage();
                };

                $scope.getNextPage();

            });

            //getconfig
        });
    };

    $scope.mainFunc();
    $scope.$watch('orderBy', function (newValue, oldValue) {
        $scope.mainFunc();
    });
    $scope.$watch('orderMode', function (a, b) {
        $scope.mainFunc();
    })

}]);

