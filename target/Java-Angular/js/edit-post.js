/**
 * Created by dangdien on 6/26/17.
 */

demoApp.factory('getSinglePost', ['$http', function ($http) {
    return function (postId) {
        return $http({
            method: 'GET',
            url: '/post',
            params: {id: postId}
        }).then(function (response) {
            return response.data.data.object;
        });
    }
}]);


demoApp.controller('editPostCtrl', ['$scope', '$http', 'userInfo', '$routeParams', 'getSinglePost', '$location', '$filter', function ($scope, $http, userInfo, $routeParams, getSinglePost, $location, $filter) {

    userInfo.then(function (info) {
        $scope.postId = $routeParams.ID;
        $scope.alerts = [];
        getSinglePost($scope.postId).then(function (postData) {
            if(!postData) {
                $location.path("/404")
            }
            $scope.selectedPost = postData;
            $scope.selectedPost.lastUpdateAuthor = info.username;
        });
    });

    $scope.changePrivacy = function (value) {
        $scope.selectedPost.status = value;
    };


    $scope.updatePost = function () {
        if (!$scope.selectedPost.title) {
            $scope.alerts = [];
            $scope.alerts.push({type: 'danger', msg: $filter('translate')('TITLE_NULL_ERROR')});
        }
        else if (!$scope.selectedPost.content) {
            $scope.alerts = [];
            $scope.alerts.push({type: 'danger', msg: $filter('translate')('CONTENT_NULL_ERROR')});
        }
        else {
            $http.post('/user/update-post', $scope.selectedPost).then(function (response) {
                $location.path("/view-single-post/" + $scope.postId);
            })
        }
    }
}]);