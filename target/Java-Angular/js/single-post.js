/**
 * Created by dangdien on 6/27/17.
 */
demoApp.controller("singlePostCtrl", ['$scope', 'getSinglePost', '$routeParams', '$http', '$cookies', '$location', function ($scope, getSinglePost, $routeParams, $http, $cookies, $location) {
    $scope.postId = $routeParams.ID;
    $scope.likeObject = {array: []};
    $scope.like = false;
    $http({
        method: 'GET',
        url: "/getconfig"
    }).then(function (response) {
        $scope.pageConfig = response.data.data.object;
    });

    $scope.increaseLike = function (value) {
        $http({
            method: 'GET',
            url: "/increase-like",
            params: {
                id: $scope.postId,
                value: value
            }
        }).then(function (response) {
            $scope.currentPost.noLike = response.data.data.object;
        });
    };

        $http({
            method: 'GET',
            url: "/increase-view",
            params: {
                id: $scope.postId
            }
        }).then(function (response) {

        getSinglePost($scope.postId).then(function (postData) {
            if (!postData) {
                $location.path("/404")
            }
            $scope.currentPost = postData;
            $scope.reload = function () {
                if ($cookies.get("like")) {
                    $scope.likeObject = JSON.parse($cookies.get("like"));
                }

                for (i in $scope.likeObject.array) {
                    if ($scope.likeObject.array[i] == parseInt($scope.currentPost.id)) {
                        $scope.like = true;
                        $scope.likeToggle = function () {
                            $scope.likeObject.array.splice(i, 1);
                            $cookies.putObject("like", $scope.likeObject.id);
                            $scope.like = false;
                            $scope.increaseLike(-1);
                            $scope.reload();
                        };
                        break;
                    }
                }

                if ($scope.like == false) {
                    $scope.likeToggle = function () {
                        $scope.likeObject.array.push($scope.currentPost.id);
                        $cookies.putObject("like", $scope.likeObject);
                        $scope.like = true;
                        $scope.increaseLike(1);
                        $scope.reload();
                    }
                }
            };
            $scope.reload();
        });
    });

}]);