/**
 * Created by dangdien on 7/5/17.
 */
/**
 * Created by dangdien on 6/24/17.
 */
demoApp.controller("userPostCtrl", ['$scope', '$http', 'userInfo', 'editPost', '$sce','$filter','$routeParams', '$window', function ($scope, $http, userInfo, editPost, $sce, $filter, $routeParams, $window) {
    $scope.pageOffset = 0;
    $scope.blogList = [];
    $scope.nextList = [];
    $scope.orderBy = "post_time";
    $scope.orderMode = "DESC";
    $scope.orderModeBool = true;
    $scope.alerts = [];
    $scope.page = 1;


    $scope.deletePost = function (id) {
        $http({
            method: 'POST',
            url: "/user/delete-post",
            params: {id: id}
        }).then(function (response) {
            if(response.data.status == "SUCCESS") {
                $scope.alerts = [];
                $scope.alerts.push({msg: $filter('translate')('DELETE_POST_SUCCESS')});
                $scope.mainFunc();
            }
            else {
                $scope.alerts = [];
                $scope.alerts.push({msg: $filter('translate')(response.data.message)});
            }
        });
    };

    $scope.closeAlert = function(index) {
        $scope.alerts.splice(index, 1);
    };



    $scope.setEditPost = function (post) {
        editPost.setPost(post);
    };

    $scope.changeOrderMode = function (value) {
        if($scope.orderBy == value) {
            $scope.orderModeBool = !$scope.orderModeBool;
        }
        if($scope.orderModeBool == true) {$scope.orderMode = "DESC"}
        else {$scope.orderMode = "ASC"}
    };

    $scope.search = function () {
        $scope.mainFunc();
    };

    $scope.mainFunc = function () {
        $http.get('/getconfig').then(function (config) {
            $scope.pageConfig = config.data.data.object;
            userInfo.then(function (info) {
                $scope.username = $routeParams.username;
                $scope.getBlogList = function (offset) {
                    if(!$scope.keyword) {
                        return $http({
                            method: 'GET',
                            url: '/author/blog-list',
                            params: {
                                limit: $scope.pageConfig[2].value,
                                orderBy: $scope.orderBy,
                                orderMode: $scope.orderMode,
                                offset: offset,
                                username: $scope.username
                            }
                        }).then(function (response) {
                            return response.data.data.object;
                        });
                        //get user blog list
                    }
                    else {
                        return $http({
                            method: 'GET',
                            url: '/author/search',
                            params: {
                                limit: $scope.pageConfig[2].value,
                                orderBy: $scope.orderBy,
                                orderMode: $scope.orderMode,
                                offset: offset,
                                username: $scope.username,
                                keyword: "%"+$scope.keyword+"%"
                            }
                        }).then(function (response) {
                            return response.data.data.object;
                        });
                    }
                };
                //get user info

                //PAGINATION
                $scope.getBlogList($scope.pageOffset).then(function (data) {

                    $scope.blogList = data;
                    $scope.getNextPage = function () {
                        $scope.pageOffsetNext = $scope.pageOffset + parseInt($scope.pageConfig[2].value);
                        $scope.getBlogList($scope.pageOffsetNext).then(function (nextData) {
                            $scope.nextList = nextData;

                            if ($scope.nextList.length) {
                                $scope.nextPage = function () {
                                    $scope.blogList = $scope.nextList;
                                    $scope.pageOffset += parseInt($scope.pageConfig[2].value);
                                    $scope.getNextPage();
                                    $scope.page++;
                                    $window.scrollTo(0, 0);
                                };
                            }
                        });
                    };

                    $scope.prevPage = function () {
                        $scope.page--;
                        $scope.pageOffset -= parseInt($scope.pageConfig[2].value);
                        $scope.getBlogList($scope.pageOffset).then(function (data) {
                            $scope.blogList = data;
                        });
                        $window.scrollTo(0, 0);
                        $scope.getNextPage();
                    };

                    $scope.getNextPage();

                });
            })
            //getconfig
        });
    };

    $scope.mainFunc();
    $scope.$watch('orderBy', function (newValue, oldValue) {
        $scope.mainFunc();
    });
    $scope.$watch('orderMode', function (a,b) {
        $scope.mainFunc();
    })

}]);

demoApp.factory('editPost', [function () {
    var post = {};
    return {
        getPost: function () {
            return post;
        },
        setPost: function (newPost) {
            post = newPost;
        }
    }
}]);