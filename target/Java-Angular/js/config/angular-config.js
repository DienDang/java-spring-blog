/**
 * Created by dangdien on 7/1/17.
 */

demoApp.config(function ($routeProvider, $translateProvider) {
    $routeProvider.when("/", {
        templateUrl: "home"
    })
        .when("/login", {
            templateUrl: "login"
        })
        .when("/write-post", {
            templateUrl: "user/write-post"
        })
        .when("/blog-list", {
            templateUrl: "blog-list"
        })
        .when("/user-dashboard", {
            templateUrl: "user/dashboard"
        })
        .when("/edit-post/:ID", {
            templateUrl: function (param) {
                return "user/edit-post?id=" + param.ID;
            }
        })
        .when("/view-single-post/:ID", {
            templateUrl: "/directives/single-post.html"
        })
        .when("/admin/dashboard", {
            templateUrl: "/admin/dashboard"
        })
        .when("/admin/config", {
            templateUrl: "/admin/config"
        })
        .when("/admin/user", {
            templateUrl: "/admin/user"
        })
        .when("/admin/roles", {
            templateUrl: "/admin/roles"
        })
        .when("/login-error", {
            templateUrl: "directives/login-error.html"
        })
        .when("/author/:username", {
        templateUrl: "/directives/user-posts.html"
        })
        .when("/404", {
            templateUrl: "/directives/404.html"
        })
        .otherwise({redirectTo: '/404'})
    ;

    $translateProvider.translations('en', translationEN);
    $translateProvider.translations('vi', translationVN);
    $translateProvider.preferredLanguage('vi');
    $translateProvider.fallbackLanguage('vi');
});