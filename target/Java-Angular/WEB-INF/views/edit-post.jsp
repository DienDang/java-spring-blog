<%--
  Created by IntelliJ IDEA.
  User: dangdien
  Date: 6/26/17
  Time: 11:34 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<div class="container mt40" ng-controller="editPostCtrl" ng-show="selectedPost">
    <div uib-alert ng-repeat="alert in alerts" ng-class="'alert-' + (alert.type || 'warning')" close="closeAlert($index)">{{alert.msg}}</div>
    <div>
        <label for="title">{{'TITLE' | translate}}</label>
        <input type="text" class="form-control" id="title" ng-model="selectedPost.title">
    </div>

    <!--<div>-->
    <!--<label for="title">Upload file:</label>-->
    <!--<input type="file" file-model="myFile"/>-->
    <!--</div>-->

    <div class="mt10">
        <label for="image">{{'IMAGE' | translate}}</label>
        <input type="text" class="form-control" id="image" ng-model="selectedPost.imageLink">
    </div>

    <div class="mt10">
    <label>{{'STATUS' | translate}} : </label>
    <div class="dropdown">
        <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">{{selectedPost.status == 0 ? 'Private' : 'Public'}}
            <span class="caret"></span></button>
        <ul class="dropdown-menu">
            <li><a href="" ng-click="changePrivacy(false)">Private</a></li>
            <li><a href="" ng-click="changePrivacy(true)">Public</a></li>
        </ul>
    </div>
    </div>

    <div class="mt40">
        <div text-angular ng-model="selectedPost.content"></div>
    </div>
    <div class="text-right mt40">
        <button ng-click="updatePost()" type="button" class="btn btn-success write-button"><a href="">{{'UPDATE' | translate}}</a>
        </button>
    </div>
</div>

