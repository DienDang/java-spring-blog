<%--
  Created by IntelliJ IDEA.
  User: dangdien
  Date: 6/28/17
  Time: 9:23 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<div id="wrapper" ng-init="open = false" ng-class="{'toggled': open}" ng-controller="adminDashboard">
    <div class="overlay" ng-class="{'opened' :open, 'closed': !open}"></div>

    <!-- Sidebar -->
    <nav class="navbar navbar-inverse navbar-fixed-top" id="sidebar-wrapper" role="navigation">
        <ul class="nav sidebar-nav">
            <li class="sidebar-brand">
                <a href="#/test">
                    Brand
                </a>
            </li>
            <li>
                <a href="#!admin/dashboard">{{'POST' | translate}}</a>
            </li>
            <li>
                <a href="#!admin/user">{{'AUTHORS' | translate}}</a>
            </li>
            <li>
                <a href="#!admin/config">{{'CONFIG' | translate}}</a>
            </li>
        </ul>
    </nav>
    <!-- /#sidebar-wrapper -->

    <!-- Page Content -->
    <div id="page-content-wrapper">
        <button type="button" class="hamburger" ng-class="{'is-open' :open, 'is-closed': !open}" ng-click="open = !open"
                data-toggle="offcanvas">
            <span class="hamb-top"></span>
            <span class="hamb-middle"></span>
            <span class="hamb-bottom"></span>
        </button>
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="pl20 pr20">
                        <div uib-alert ng-repeat="alert in alerts" ng-class="'alert-' + (alert.type || 'warning')"
                             close="closeAlert($index)">{{alert.msg}}
                        </div>
                        <h5>{{'NO_APPROVE' | translate}} : {{approved}}</h5>
                        <h5>{{'NO_UN_APPROVE' | translate}} : {{notApproved}}</h5>

                        <form>
                        <label>{{'FILTER' | translate}}</label>
                        <div class="dropdown" style="display: inline-block">
                            <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">
                                {{searchOption == 'title' ? 'TITLE' : 'AUTHORS' | translate}}
                                <span class="caret"></span></button>
                            <ul class="dropdown-menu">
                                <li><a href="" ng-click="changeMode('title')">{{'TITLE' | translate}}</a></li>
                                <li><a href="" ng-click="changeMode('author')">{{'AUTHORS' | translate}}</a></li>
                            </ul>
                        </div>
                            <input type="text" class="form-control" style="width: 200px; display: inline-block"
                                   ng-model="keyword">
                            <button class="btn btn-default" type="submit" ng-click="search()">{{'FILTER' | translate}}
                            </button>
                            <button class="btn btn-default" ng-click="keyword = '';search()" ng-show="keyword">Reset
                            </button>
                        </form>
                        <table class="table table-striped mt20">
                            <thead>
                            <tr>
                                <th>
                                    <a href="" class="table-heading" ng-click="orderBy = 'title'; changeOrderMode('title')">{{'TITLE' |
                                        translate}}

                                    <i ng-class="{'fa fa-long-arrow-down' : orderMode == 'DESC', 'fa fa-long-arrow-up' : orderMode == 'ASC'}" aria-hidden="true" ng-show="orderBy == 'title'"></i>
                                    </a>
                                </th>
                                <th>
                                    <a href="" class="table-heading" ng-click="orderBy = 'username'; changeOrderMode('username')">{{'AUTHORS'
                                        | translate}}

                                    <i ng-class="{'fa fa-long-arrow-down' : orderMode == 'DESC', 'fa fa-long-arrow-up' : orderMode == 'ASC'}" aria-hidden="true" ng-show="orderBy == 'username'"></i>
                                    </a>

                                </th>
                                <th>
                                    <a href="" class="table-heading" ng-click="orderBy = 'post_time'; changeOrderMode('post_time')">{{'POST_TIME'
                                        | translate}}

                                    <i ng-class="{'fa fa-long-arrow-down' : orderMode == 'DESC', 'fa fa-long-arrow-up' : orderMode == 'ASC'}" aria-hidden="true" ng-show="orderBy == 'post_time'"></i>
                                    </a>

                                </th>
                                <th>
                                    <a href="" class="table-heading" ng-click="orderBy = 'no_view'; changeOrderMode('no_view')">{{'VIEW' |
                                        translate}}

                                    <i ng-class="{'fa fa-long-arrow-down' : orderMode == 'DESC', 'fa fa-long-arrow-up' : orderMode == 'ASC'}" aria-hidden="true" ng-show="orderBy == 'no_view'"></i>
                                    </a>

                                </th>
                                <th>
                                    <a href="" class="table-heading" ng-click="orderBy = 'status'; changeOrderMode('status')">{{'STATUS' |
                                        translate}}

                                    <i ng-class="{'fa fa-long-arrow-down' : orderMode == 'DESC', 'fa fa-long-arrow-up' : orderMode == 'ASC'}" aria-hidden="true" ng-show="orderBy == 'status'"></i>
                                    </a>

                                </th>
                                <th></th>
                                <th></th>
                                <th>
                                    <a href="" class="table-heading" ng-click="orderBy = 'approve'; changeOrderMode('approve')">{{'APPROVE' |
                                        translate}}

                                    <i style="display: inline-block" ng-class="{'fa fa-long-arrow-down' : orderMode == 'DESC', 'fa fa-long-arrow-up' : orderMode == 'ASC'}" aria-hidden="true" ng-show="orderBy == 'approve'"></i>
                                    </a>

                                </th>
                            </tr>
                            </thead>

                            <tbody>
                            <tr ng-repeat="post in blogList">
                                <td class="title-column"><a href="#!/view-single-post/{{post.id}}">{{post.title}}</a>
                                </td>
                                <td>{{post.author.username}}</td>
                                <td>{{post.postTime | date: pageConfig[1].value}}</td>
                                <%--<td>{{post.lastUpdate | date: pageConfig[1].value}}</td>--%>
                                <%--<td>{{post.noLike}}</td>--%>
                                <td>{{post.noView}}</td>
                                <td>{{post.status == 1 ? 'PUBLIC' : 'PRIVATE' | translate}}</td>
                                <td><a href="#!edit-post/{{post.id}}" title="Edit"><span
                                        class="glyphicon glyphicon-pencil"></span></a></td>
                                <td><a href="" mwl-confirm
                                       confirm-text="{{'YES' | translate}}"
                                       message="{{'CONFIRM_MSG' | translate}}"
                                       placement="left"
                                       cancel-text="{{'NO' | translate}}"
                                       on-confirm="$parent.deletePost(post.id)"
                                       on-cancel=""
                                       confirm-button-type="danger"
                                       cancel-button-type="default" title="Delete"><span
                                        class="glyphicon glyphicon-trash"></span></a></td>
                                <td>
                                    <a href="" ng-click="changeApprove(post)" title="{{post.approve ? 'DisApprove' : 'Approve'}}">
                                        <span ng-class="{'glyphicon glyphicon-remove': post.approve, 'glyphicon glyphicon-ok': !post.approve, 'approve-disabled': !post.status}"></span>
                                    </a>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                        <div class="text-center"><h5>{{page}}</h5></div>
                        <ul class="pager page-menu">
                            <li class="previous">
                                <a href="" ng-click="prevPage()" ng-show="pageOffset">&larr; {{'NEWER' | translate}}</a>
                            </li>
                            <li class="next">
                                <a href="" ng-click="nextPage()" ng-show="blogList && nextList.length">{{'OLDER' |
                                    translate}} &rarr;</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /#page-content-wrapper -->

</div>
<!-- /#wrapper -->
