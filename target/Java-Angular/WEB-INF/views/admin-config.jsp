<%--
  Created by IntelliJ IDEA.
  User: dangdien
  Date: 6/28/17
  Time: 11:06 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<div id="wrapper" ng-init="open = false" ng-class="{'toggled': open}" ng-controller="configController">
    <div class="overlay" ng-class="{'opened' :open, 'closed': !open}"></div>

    <!-- Sidebar -->
    <nav class="navbar navbar-inverse navbar-fixed-top" id="sidebar-wrapper" role="navigation">
        <ul class="nav sidebar-nav">
            <li class="sidebar-brand">
                <a href="#/test">
                    Brand
                </a>
            </li>
            <li>
                <a href="#!admin/dashboard">{{'BLOG_MNG' | translate}}</a>
            </li>
            <li>
                <a href="#!/admin/user">{{'AUTHORS' | translate}}</a>
            </li>
            <li>
                <a href="#!admin/config">{{'CONFIG' | translate}}</a>
            </li>
        </ul>
    </nav>
    <!-- /#sidebar-wrapper -->

    <!-- Page Content -->
    <div id="page-content-wrapper">
        <button type="button" class="hamburger" ng-class="{'is-open' :open, 'is-closed': !open}" ng-click="open = !open"
                data-toggle="offcanvas">
            <span class="hamb-top"></span>
            <span class="hamb-middle"></span>
            <span class="hamb-bottom"></span>
        </button>
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-lg-offset-2">
                    <div uib-alert ng-repeat="alert in alerts" ng-class="'alert-' + (alert.type || 'warning')" close="closeAlert($index)">{{alert.msg}}</div>
                    <div class="pb100">
                        <div class="form-group">
                            <label for="blog-title">{{'BLOG_TITLE' | translate}}</label>
                            <input type="text" id="blog-title" class="form-control" ng-model="blogTitle">
                        </div>
                        <div class="form-group">
                            <%--<label for="date-format">{{'DATE_FORMAT' | translate}}</label>--%>
                            <%--<select name="date-format" id="date-format"--%>
                                    <%--ng-options="option.format for option in data.availableOptions track by option.id"--%>
                                    <%--ng-model="data.selectedOption"></select>--%>
                                <label>{{'DATE_FORMAT' | translate}}</label>
                                <div class="dropdown">
                                    <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">{{dateFormat}}
                                        <span class="caret"></span></button>
                                    <ul class="dropdown-menu">
                                        <li><a href="" ng-click="changeDateFormat('hh:MM:ss dd/MM/yyyy')">hh:MM:ss dd/MM/yyyy</a></li>
                                        <li><a href="" ng-click="changeDateFormat('dd/MM/yyyy hh:MM:ss')">dd/MM/yyyy hh:MM:ss</a></li>
                                        <li><a href="" ng-click="changeDateFormat('MM/dd/yyyy hh:MM:ss')">MM/dd/yyyy hh:MM:ss</a></li>
                                        <li><a href="" ng-click="changeDateFormat('yyyy/dd/MM hh:MM:ss')">yyyy/dd/MM hh:MM:ss</a></li>
                                    </ul>
                                </div>
                        </div>
                        <div class="form-group">
                            <label for="post-number">{{'VIEW_POST' | translate}}</label>
                            <input type="number" id="post-number" min="1" class="form-control" ng-model="postNumber">
                        </div>
                        <button class="btn btn-info" ng-click="updateConfig()">{{'UPDATE' | translate}}</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /#page-content-wrapper -->

</div>