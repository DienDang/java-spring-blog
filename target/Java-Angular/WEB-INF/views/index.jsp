<%--
  Created by IntelliJ IDEA.
  User: dangdien
  Date: 5/6/17
  Time: 9:42 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<jsp:include page="templates/header.jsp"/>
<div ng-controller="mainCtrl">
<!-- Navigation -->
<jsp:include page="templates/navbar.jsp"/>
<!-- Page Content -->
    <div ng-view ng-cloak></div>
</div>

<jsp:include page="templates/footer.jsp"/>
<!-- /.container -->

