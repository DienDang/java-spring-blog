<%--
  Created by IntelliJ IDEA.
  User: dangdien
  Date: 6/24/17
  Time: 2:19 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<div class="container" ng-controller="userDashboardCtrl">
    <div class="row">
        <div>
            <div uib-alert ng-repeat="alert in alerts" ng-class="'alert-' + (alert.type || 'warning')" close="closeAlert($index)">{{alert.msg}}</div>
            <form>
            <div class="pb20">
                <label>{{'FILTER' | translate}}</label>
                <input type="text" style="width: 200px; display: inline-block"  class="form-control" ng-model="keyword">
                <button class="btn btn-default" ng-click="search()" type="submit">{{'FILTER' | translate}}</button>
                <button class="btn btn-default" ng-click="keyword = '';search()" ng-show="keyword">Reset</button>
            </div>
            </form>
        <table class="table table-striped">
            <thead>
                <tr>
                    <th>
                        <a href="" class="table-heading" ng-click="orderBy = 'title'; changeOrderMode('title')">{{'TITLE' | translate}}
                            <i ng-class="{'fa fa-long-arrow-down' : orderMode == 'DESC', 'fa fa-long-arrow-up' : orderMode == 'ASC'}" aria-hidden="true" ng-show="orderBy == 'title'"></i>

                        </a>
                    </th>
                    <th>
                        <a href="" class="table-heading" ng-click="orderBy = 'approve'; changeOrderMode('approve')">{{'APPROVE' | translate}}
                            <i ng-class="{'fa fa-long-arrow-down' : orderMode == 'DESC', 'fa fa-long-arrow-up' : orderMode == 'ASC'}" aria-hidden="true" ng-show="orderBy == 'approve'"></i>

                        </a>
                    </th>
                    <th>
                        <a href="" class="table-heading" ng-click="orderBy = 'post_time'; changeOrderMode('post_time')">{{'POST_TIME' | translate}}
                            <i ng-class="{'fa fa-long-arrow-down' : orderMode == 'DESC', 'fa fa-long-arrow-up' : orderMode == 'ASC'}" aria-hidden="true" ng-show="orderBy == 'post_time'"></i>

                        </a>
                    </th>
                    <th>
                        <a href="" class="table-heading" ng-click="orderBy = 'last_update_time'; changeOrderMode('last_update_time')">{{'LAST_EDIT' | translate}}
                            <i ng-class="{'fa fa-long-arrow-down' : orderMode == 'DESC', 'fa fa-long-arrow-up' : orderMode == 'ASC'}" aria-hidden="true" ng-show="orderBy == 'last_update_time'"></i>

                        </a>
                    </th>
                    <%--<th>--%>
                        <%--<a href="" ng-click="orderBy = 'no_like'; changeOrderMode('no_like')">{{'LIKE' | translate}}</a>--%>
                    <%--</th>--%>
                    <th>
                        <a href="" class="table-heading" ng-click="orderBy = 'no_view'; changeOrderMode('no_view')">{{'VIEW' | translate}}
                            <i ng-class="{'fa fa-long-arrow-down' : orderMode == 'DESC', 'fa fa-long-arrow-up' : orderMode == 'ASC'}" aria-hidden="true" ng-show="orderBy == 'no_view'"></i>

                        </a>
                    </th>
                    <th>
                        <a href="" class="table-heading" ng-click="orderBy = 'status'; changeOrderMode('status')">{{'STATUS' | translate}}
                            <i ng-class="{'fa fa-long-arrow-down' : orderMode == 'DESC', 'fa fa-long-arrow-up' : orderMode == 'ASC'}" aria-hidden="true" ng-show="orderBy == 'status'"></i>

                        </a>
                    </th>
                    <th></th>
                    <th></th>
                </tr>
            </thead>

            <tbody>
                <tr ng-repeat="post in blogList">
                    <td class="title-column"><a href="#!/view-single-post/{{post.id}}">{{post.title}}</a></td>
                    <td>{{post.approve == 1 ? 'APPROVED' : 'NOT_APPROVED' | translate}}</td>
                    <td>{{post.postTime | date: pageConfig[1].value}}</td>
                    <td>{{post.lastUpdate | date: pageConfig[1].value}}</td>
                    <%--<td>{{post.noLike}}</td>--%>
                    <td>{{post.noView}}</td>
                    <td>{{post.status == 1 ? 'PUBLIC' : 'PRIVATE' | translate}}</td>
                    <td><a href="#!edit-post/{{post.id}}" title="Edit"><span class="glyphicon glyphicon-pencil"></span></a></td>
                    <td><a href="" mwl-confirm
                           confirm-text="{{'YES' | translate}}"
                           message="{{'CONFIRM_MSG' | translate}}"
                           placement="left"
                           cancel-text="{{'NO' | translate}}"
                           on-confirm="$parent.deletePost(post.id)"
                           on-cancel=""
                           confirm-button-type="danger"
                           cancel-button-type="default" title="Delete"><span class="glyphicon glyphicon-trash"></span></a></td>
                </tr>
            </tbody>
        </table>
            <div class="text-center"><h5>{{page}}</h5></div>
        <ul class="pager">
            <li class="previous">
                <a href="" ng-click="prevPage()" ng-show="pageOffset">&larr;{{'NEWER' | translate}}</a>
            </li>
            <li class="next">
                <a href="" ng-click="nextPage()" ng-show="blogList && nextList.length">{{'OLDER' | translate}} &rarr;</a>
            </li>
        </ul>
    </div>
    </div>
</div>
