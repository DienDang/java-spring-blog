<%--
  Created by IntelliJ IDEA.
  User: dangdien
  Date: 6/19/17
  Time: 4:33 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<div class="container">
    <div class="row">
        <!-- Blog Entries Column -->
        <div class="col-md-8">
            <!-- First Blog Post -->
            <div ng-repeat="post in blogList">
                <h2>
                    <a href="#!view-single-post/{{post.id}}">{{post.title}}</a>
                </h2>
                <p>
                    {{'BY' | translate}} <a href="#!/author/{{post.author.username}}">{{post.author.username}}</a> {{'POST_ON' | translate}} {{post.postTime | date:
                    pageConfig[1].value}}
                </p>
                <p><i class="fa fa-thumbs-up" aria-hidden="true"></i> {{post.noLike}}<span class="ml15"><i class="fa fa-eye" aria-hidden="true"></i> {{post.noView}}</span></p>
                <hr>
                <img class="img-responsive" src="{{post.imageLink}}" alt="">
                <hr>
                <div ng-bind-html="post.content"></div>
                <a class="btn btn-primary" href="#!view-single-post/{{post.id}}">{{'READMORE' | translate}} <span
                        class="glyphicon glyphicon-chevron-right"></span></a>

                <hr>
            </div>
            <!-- Pager -->
            <div class="text-center"><h5>{{page}}</h5></div>
            <ul class="pager">
                <li class="previous">
                    <a href="#" ng-click="prevPage()" ng-show="pageOffset">&larr; {{'NEWER' | translate}}</a>
                </li>
                <li class="next">
                    <a href="#" ng-click="nextPage()" ng-show="blogList && nextList.length">{{'OLDER' | translate}}
                        &rarr;</a>
                </li>
            </ul>

        </div>

        <!-- Blog Sidebar Widgets Column -->
        <div class="col-md-4">

            <!-- Blog Search Well -->
            <div class="well">
                <form>
                <h4>Blog Search</h4>
                <div class="input-group">
                    <input type="text" class="form-control" ng-model="$parent.keyword">
                    <span class="input-group-btn">
                        <button class="btn btn-default" type="submit" ng-click="search(); $parent.searchShow = true" style="height: 34px;">
                                <span class="glyphicon glyphicon-search"></span>
                        </button>
                        <button class="btn btn-default" ng-click="$parent.keyword = '';search(); $parent.searchShow = false" ng-show="$parent.keyword">Reset</button>
                        </span>
                </div>
                <p class="pt10" ng-show="searchShow">Kết quả tìm kiếm: {{searchSize}} bài viết</p>
                <!-- /.input-group -->
                </form>
            </div>

        </div>

    </div>
    <!-- /.row -->

    <hr>

</div>